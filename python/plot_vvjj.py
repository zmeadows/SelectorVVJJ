from plot_util import *
from plot_loader import *
from ROOT import *

# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15HadronicPythiaVJets
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15HadronicHerwigVJets
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15HadronicWJets

##########################
### VARIABLE TEX NAMES ###
##########################

AXIS_TITLES = {
        "h_lead_jet_pt"     : "Leading jet p_{T} [GeV]",
        "h_lead_jet_eta"    : "Leading jet #eta",
        "h_lead_jet_phi"    : "Leading jet #phi",
        "h_lead_jet_m"      : "Leading jet m_{comb} [GeV]",
        "h_lead_jet_D2"     : "Leading jet D2",
        "h_lead_jet_ntrk"   : "Leading jet n_{trk}",
        "h_sub_jet_pt"      : "Subleading jet p_{T} [GeV]",
        "h_sub_jet_eta"     : "Subleading jet #eta",
        "h_sub_jet_phi"     : "Subleading jet #phi",
        "h_sub_jet_m"       : "Subleading jet m_{comb} [GeV]",
        "h_sub_jet_D2"      : "Subleading jet D2",
        "h_sub_jet_ntrk"    : "Subleading jet n_{trk}",
        "h_first_jet_pt"    : "First mass-ordered jet p_{T} [GeV]",
        "h_first_jet_eta"   : "First mass-ordered jet #eta",
        "h_first_jet_phi"   : "First mass-ordered jet #phi",
        "h_first_jet_m"     : "First mass-ordered jet m_{comb} [GeV]",
        "h_first_jet_D2"    : "First mass-ordered jet D2",
        "h_first_jet_ntrk"  : "First mass-ordered jet n_{trk}",
        "h_second_jet_pt"   : "Second mass-ordered jet p_{T} [GeV]",
        "h_second_jet_eta"  : "Second mass-ordered jet #eta",
        "h_second_jet_phi"  : "Second mass-ordered jet #phi",
        "h_second_jet_m"    : "Second mass-ordered jet m_{comb} [GeV]",
        "h_second_jet_D2"   : "Second mass-ordered jet D2",
        "h_second_jet_ntrk" : "Second mass-ordered jet n_{trk}",
        "h_mjj"             : "m_{JJ} [GeV]",
        "h_ptasym"          : "p_{T} asymmetry",
        "h_dyjj"            : "|#Delta y|",
        "h_ntrkEffNum"      : "Leading jet n_{trk}",
        "h_ptasymEffNum"    : "p_{T} asymmetry",
        "h_dyjjEffNum"      : "|#Delta y|",
        }

def get_axis_title(histo_name):
        return get_closest_match(histo_name, AXIS_TITLES)
