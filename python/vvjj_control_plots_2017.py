from ROOT import *
import atlas_style

import os
import math
from sys import argv, exit

from plot_base   import *
from plot_util   import *
from plot_loader import *
from plot_vvjj   import *
from weight_tool import *

gROOT.SetBatch()
sane_defaults()
TGaxis.SetMaxDigits(4)
gStyle.SetOptStat(0)

RAW = PlotLoader("/eos/atlas/user/z/zmeadows/VVJJ/09032017_PrePostICHEP_Ntuples/cp.merged.with1500GeV.redoneWZ2000.root")
WEIGHT_TOOL = WeightTool("/afs/cern.ch/work/z/zmeadows/public/VVJJ/SelectorVVJJ/SelectorVVJJ/data/xsections.txt")

ROOT_OUTPUT_DIR = "/afs/cern.ch/work/z/zmeadows/public/VVJJ/LocalTest/EOSBrokenSoLocalPlots_28032017"
OUTPUT_DIR = ROOT_OUTPUT_DIR
make_dir(ROOT_OUTPUT_DIR)
make_dir(OUTPUT_DIR)

MAKE_DATAMC_PLOTS = False
MAKE_SOVERB_PLOTS = False
MAKE_EFF_PLOTS    = True

SIGNAL_MASS_POINTS = [
    "1100", "1200", "1300", "1400", "1500",
    "1600", "1700", "1800", "1900", "2000", "2200", "2400",
    "2600", "2800", "3000", "3500", "4000", "4500", "5000"
    ]

MJJ_MASS_WINDOWS = ["1500", "2000", "2400", "3000", "4000", "5000"]

SIGNAL_SAMPLES = ["HVT_Agv1_VcWZ_qqqq", "HVT_Agv1_VzWW_qqqq", "RS_G_WW_qqqq_c10", "RS_G_ZZ_qqqq_c10"]

# {{{
class PlotSimpleDataMC(PlotBase):
    def __init__(self, var_name, signal_type, flip_legend = False, **kwargs):

        super(PlotSimpleDataMC, self).__init__(
                lumi_val = "31.7",
                width = 600,
                name = var_name + "_" + signal_type + "_DataBkgSig",
                atlas_mod = "Internal",
                legend_loc = [0.20,0.93,0.56,0.55] if flip_legend else [0.65,0.93,0.91,0.74],
                atlas_loc = [0.6,0.9] if flip_legend else None,
                extra_lines_loc = [0.6,0.8] if flip_legend else None,
                x_title = get_axis_title(var_name),
                x_units = "",
                **kwargs)

        # GET PLOTS FROM FILE

        masspoint_str = signal_type[-4:]

        h_data = RAW.get_hist(["data16", "nominal"] , var_name)
        h_bkg = RAW.get_hist(["qcd", "nominal"] , var_name)
        h_signal = RAW.get_hist([signal_type, "nominal"] , var_name)

        if (not h_data or not h_bkg or not h_signal):
            return

        h_bkg.Scale(h_data.Integral() / h_bkg.Integral())
        h_signal.Scale(h_data.Integral() / h_signal.Integral())

        all_hists = [h_data, h_signal, h_bkg]

        if (self.rebin != None):
            h_data.Rebin(self.rebin)
            h_bkg.Rebin(self.rebin)
            h_signal.Rebin(self.rebin)

        self.determine_y_axis_title(h_data)

        for h in all_hists:
            h.GetYaxis().SetTitle(self.y_title)
            h.GetXaxis().SetLabelSize(0)
            h.GetYaxis().SetTitleOffset(2.0)
            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            fill_overflow(h)

        self.pad_empty_space(all_hists)

        # CREATE RATIO PLOTS

        h_ratio = h_bkg.Clone("h_ratio")
        h_ratio.Divide(h_data, h_ratio, 1, 1, "")

        ratio_title = "Data/MC"
        set_style_ratio(h_ratio, y_title = ratio_title)
        h_ratio.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
        self.set_x_axis_bounds(h_ratio)
        h_ratio.GetYaxis().SetTitleOffset(2.0)
        h_ratio.GetXaxis().SetTitleOffset(4.0)
        h_ratio.GetXaxis().SetLabelSize(19)

        # if self.log_scale: self.name += "_log"

        # TODO: necessary?
        self.canvas.Clear()
        self._make_canvas()
        self._make_decorations()

        # set_data_style_simple(h_data)
        # set_data_style_simple(h_ratio)
        set_mc_style_simple_hist(h_bkg, kRed)
        set_mc_style_line(h_signal, kGreen+2, line_width = 2)

        # SET UP THE CANVAS
        self.canvas.Divide(1,2)
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.33, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.00)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad2 = self.canvas.cd(2)
        self.pad2.SetPad(0.0, 0.0, 1., 0.33)
        self.pad2.SetTopMargin(0.05)
        self.pad2.SetRightMargin(0.07)
        self.pad2.SetBottomMargin(0.3)
        self.pad2.SetFillColorAlpha(0, 0.)
        self.pad2.SetGridy(1) # grid for ratio plot
        self.pad2.SetBorderSize(0)

        self.pad1.cd()

        h_bkg.Draw("hist")
        h_data.Draw("PE,same")
        h_signal.Draw("hist,same")

        self.pad1.RedrawAxis()

        self.canvas.cd()
        self.leg.AddEntry(h_data, "Data 2016")
        self.leg.AddEntry(h_bkg, "multijet")
        self.leg.AddEntry(h_signal, "m_{W'} = " + masspoint_str + " GeV")
        self.leg.Draw()

        self.pad2.cd()

        h_ratio.Draw("PE,same")
        self.pad2.RedrawAxis("g")
        self.pad1.RedrawAxis()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".pdf")
        self.canvas.Clear()

# }}}

# {{{
class PlotSoverB(PlotBase):
    def __init__(self, var_name, signal_type, flip_legend = False, **kwargs):

        super(PlotSoverB, self).__init__(
                lumi_val = "X.X",
                width = 600,
                tex_size_mod = 1.4,
                hide_lumi = True,
                name = var_name + "_" + signal_type + "_SoverB",
                atlas_mod = "Internal",
                legend_loc = [0.20,0.93,0.56,0.55] if flip_legend else [0.55,0.90,0.88,0.67],
                atlas_loc = [0.6,0.9] if flip_legend else [0.2,0.83],
                extra_lines_loc = [0.6,0.8] if flip_legend else [0.2,0.73],
                x_title = get_axis_title(var_name),
                x_units = "",
                **kwargs)

        # GET PLOTS FROM FILE

        masspoint_str = signal_type[-4:]

        h_bkg = RAW.get_hist(["qcd", "nominal"] , var_name)
        h_signal = RAW.get_hist([signal_type, "nominal"] , var_name)

        if (not h_bkg or not h_signal): return

        if "ntrk" in var_name:
            print var_name
            print var_name.replace("ntrkEffNum","lead_jet_ntrk")
            h_sig_norm = RAW.get_hist([signal_type, "nominal"] , var_name.replace("ntrkEffNum","lead_jet_ntrk"))
            sig_norm_factor = h_sig_norm.Integral(0,h_sig_norm.GetSize()+1)
            h_bkg_norm = RAW.get_hist(["qcd", "nominal"] , var_name.replace("ntrkEffNum","lead_jet_ntrk"))
            bkg_norm_factor = h_bkg_norm.Integral(0,h_bkg_norm.GetSize()+1)
        else:
            h_sig_norm = RAW.get_hist([signal_type, "nominal"] , var_name.replace("EffNum",""))
            sig_norm_factor = h_sig_norm.Integral(0,h_sig_norm.GetSize()+1)
            h_bkg_norm = RAW.get_hist(["qcd", "nominal"] , var_name.replace("EffNum",""))
            bkg_norm_factor = h_bkg_norm.Integral(0,h_bkg_norm.GetSize()+1)

        h_signal_eff = h_signal.Clone(h_signal.GetName() + "_eff")
        h_bkg_eff = h_bkg.Clone(h_bkg.GetName() + "_eff")

        def make_eff(h_num, norm):
            for ibin in range(h_num.GetSize()):
                h_num.SetBinContent(ibin, h_num.GetBinContent(ibin) / norm)

        make_eff(h_bkg_eff, bkg_norm_factor)
        make_eff(h_signal_eff, sig_norm_factor)

        h_sob = h_signal_eff.Clone("h_" + self.name + "_SoverB")
        for ibin in range(h_sob.GetSize()):
            bkg_bin = h_bkg_eff.GetBinContent(ibin)
            if (bkg_bin > 0):
                h_sob.SetBinContent(ibin, h_sob.GetBinContent(ibin) / math.sqrt(bkg_bin))
            else:
                h_sob.SetBinContent(ibin, 0)

        all_hists = [h_signal_eff, h_bkg_eff, h_sob]

        self.determine_y_axis_title(h_bkg, "#varepsilon_{B}, #varepsilon_{S}, S/#sqrt{B}")

        for h in all_hists:
            h.GetYaxis().SetTitle(self.y_title)
            h.GetXaxis().SetLabelSize(0)
            h.GetYaxis().SetTitleOffset(2.0)
            h.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            h.GetXaxis().SetTitleOffset(1.5)
            h.GetXaxis().SetLabelSize(19)

        self.pad_empty_space(all_hists)

        # if self.log_scale: self.name += "_log"

        set_mc_style_line(h_sob, kGreen+2, line_width = 2)
        set_mc_style_line(h_signal_eff, kRed+2, line_width = 2)
        set_mc_style_line(h_bkg_eff, kBlue+2, line_width = 2)

        # SET UP THE CANVAS
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.1, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.10)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        self.pad1.SetGridy(1)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad1.cd()

        h_bkg_eff.Draw("hist")
        h_signal_eff.Draw("hist,same")
        h_sob.Draw("hist,same")

        self.pad1.RedrawAxis("g")

        self.canvas.cd()
        self.leg.AddEntry(h_sob, "S/#sqrt{B}")
        self.leg.AddEntry(h_bkg_eff, "#varepsilon_{B}")
        self.leg.AddEntry(h_signal_eff, "#varepsilon_{S}, m_{W'} = " + masspoint_str + " GeV")
        self.leg.Draw()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".pdf")
        self.canvas.Clear()

# }}}

class PlotMjjSignalEfficiency(PlotBase):
    def __init__(self, signal_type, flip_legend = False, **kwargs):

        super(PlotMjjSignalEfficiency, self).__init__(
                width = 600,
                tex_size_mod = 1.4,
                hide_lumi = True,
                name = signal_type + "_Eff",
                atlas_mod = "Simulation",
                extra_legend_lines = ["Internal"],
                legend_loc = [0.20,0.93,0.56,0.55] if flip_legend else [0.62,0.90,0.90,0.70],
                atlas_loc = [0.6,0.9] if flip_legend else [0.2,0.85],
                extra_lines_loc = [0.6,0.8] if flip_legend else [0.2,0.73],
                x_title = "Resonance Mass",
                x_units = "GeV",
                x_min = 1000,
                x_max = 5100,
                y_max = 1.3,
                **kwargs)

        # GET PLOTS FROM FILE

        g_exot3              = None
        g_presel             = None
        g_dyjj               = None
        g_topo               = None
        g_topo_bosontag      = None
        g_topo_bosontag_ntrk = None

        x_points = map(float, SIGNAL_MASS_POINTS)

        y_exot3              = [0.0] * len(SIGNAL_MASS_POINTS)
        y_presel             = [0.0] * len(SIGNAL_MASS_POINTS)
        y_dyjj               = [0.0] * len(SIGNAL_MASS_POINTS)
        y_topo               = [0.0] * len(SIGNAL_MASS_POINTS)
        y_topo_bosontag      = [0.0] * len(SIGNAL_MASS_POINTS)
        y_topo_bosontag_ntrk = [0.0] * len(SIGNAL_MASS_POINTS)

        num_events = {}

        for i in range(len(SIGNAL_MASS_POINTS)):
            if (float(SIGNAL_MASS_POINTS[i]) > self.x_max):
                x_points             = x_points[:i]
                y_exot3              = y_exot3[:i]
                y_presel             = y_presel[:i]
                y_dyjj               = y_dyjj[:i]
                y_topo               = y_topo[:i]
                y_topo_bosontag      = y_topo_bosontag[:i]
                y_topo_bosontag_ntrk = y_topo_bosontag_ntrk[:i]
                break

            folder = signal_type + "_m" + SIGNAL_MASS_POINTS[i]

            bosontag_str = ""
            bosontag_ntrk_str = ""
            if "WW" in signal_type:
                bosontag_str = "WWNoNtrk"
                bosontag_ntrk_str = "SRWW"
            elif "ZZ" in signal_type:
                bosontag_str = "ZZNoNtrk"
                bosontag_ntrk_str = "SRZZ"
            elif "WZ" in signal_type:
                bosontag_str = "WZNoNtrk"
                bosontag_ntrk_str = "SRWZ"
            else:
                print "WARNING: WW/WZ/ZZ not found in signal sample name"
                sys.exit(1)

            num_events["generated"]          = WEIGHT_TOOL.get_n_events(folder)
            num_events["exot3"]              = RAW.get_hist([folder, "nominal"] , "h_mjj_noSelection").GetEntries()
            num_events["presel"]             = RAW.get_hist([folder, "nominal"] , "h_mjj_noTopo").GetEntries()
            num_events["dyjj"]               = RAW.get_hist([folder, "nominal"] , "h_mjj_TopoNoptasym").GetEntries()
            num_events["topo"]               = RAW.get_hist([folder, "nominal"] , "h_mjj_Topo").GetEntries()
            num_events["topo_bosontag"]      = RAW.get_hist([folder, "nominal"] , "h_mjj_" + bosontag_str).GetEntries()
            num_events["topo_bosontag_ntrk"] = RAW.get_hist([folder, "nominal"] , "h_mjj_" + bosontag_ntrk_str).GetEntries()

            assert(num_events["generated"] != 0)

            y_exot3[i]              = num_events["exot3"] / num_events["generated"]
            y_presel[i]             = num_events["presel"] / num_events["generated"]
            y_dyjj[i]               = num_events["dyjj"] / num_events["generated"]
            y_topo[i]               = num_events["topo"] / num_events["generated"]
            y_topo_bosontag[i]      = num_events["topo_bosontag"] / num_events["generated"]
            y_topo_bosontag_ntrk[i] = num_events["topo_bosontag_ntrk"] / num_events["generated"]

            print folder
            print "num. generated: " , int(num_events["generated"])
            print "EXOT3: "          , int(num_events["exot3"])              , " " , round(y_exot3[i] * 100              , 2) , "%"
            print "preselection: "   , int(num_events["presel"])             , " " , round(y_presel[i] * 100             , 2) , "%"
            print "dyjj: "           , int(num_events["dyjj"])               , " " , round(y_dyjj[i] * 100               , 2) , "%"
            print "ptasym: "         , int(num_events["topo"])               , " " , round(y_topo[i] * 100               , 2) , "%"
            print "bosontag: "       , int(num_events["topo_bosontag"])      , " " , round(y_topo_bosontag[i] * 100      , 2) , "%"
            print "ntrk: "           , int(num_events["topo_bosontag_ntrk"]) , " " , round(y_topo_bosontag_ntrk[i] * 100 , 2) , "%"
            print ""

        assert(len(x_points) == len(y_exot3))
        assert(len(x_points) == len(y_presel))
        assert(len(x_points) == len(y_dyjj))
        assert(len(x_points) == len(y_topo))
        assert(len(x_points) == len(y_topo_bosontag))
        assert(len(x_points) == len(y_topo_bosontag_ntrk))

        arr_x_points             = array.array('f', x_points)
        arr_y_exot3              = array.array('f', y_exot3)
        arr_y_presel             = array.array('f', y_presel)
        arr_y_dyjj               = array.array('f', y_dyjj)
        arr_y_topo               = array.array('f', y_topo)
        arr_y_topo_bosontag      = array.array('f', y_topo_bosontag)
        arr_y_topo_bosontag_ntrk = array.array('f', y_topo_bosontag_ntrk)

        g_exot3              = TGraph(len(x_points), arr_x_points, arr_y_exot3)
        g_presel             = TGraph(len(x_points), arr_x_points, arr_y_presel)
        g_dyjj               = TGraph(len(x_points), arr_x_points, arr_y_dyjj)
        g_topo               = TGraph(len(x_points), arr_x_points, arr_y_topo)
        g_topo_bosontag      = TGraph(len(x_points), arr_x_points, arr_y_topo_bosontag)
        g_topo_bosontag_ntrk = TGraph(len(x_points), arr_x_points, arr_y_topo_bosontag_ntrk)

        all_graphs = [g_exot3, g_presel, g_dyjj, g_topo, g_topo_bosontag, g_topo_bosontag_ntrk]

        for h in all_graphs:
            h.GetHistogram().SetMinimum(0.001)
            h.GetHistogram().SetMaximum(1.5)
            if ("WZ" in signal_type and "HVT" in signal_type):
                h.GetYaxis().SetTitle("#epsilon_{HVT #rightarrow WZ}")
            elif ("WW" in signal_type and "HVT" in signal_type):
                h.GetYaxis().SetTitle("#epsilon_{HVT #rightarrow WW}")
            elif ("ZZ" in signal_type and "RS" in signal_type):
                h.GetYaxis().SetTitle("#epsilon_{G #rightarrow ZZ}")
            elif ("WW" in signal_type and "RS" in signal_type):
                h.GetYaxis().SetTitle("#epsilon_{G #rightarrow WW}")
            else:
                print "invalid signal type"
                sys.exit(1)

            h.GetXaxis().SetLabelSize(0)
            h.GetYaxis().SetTitleOffset(2.0)
            h.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            h.GetXaxis().SetRangeUser(self.x_min, self.x_max)
            h.GetXaxis().SetTitleOffset(1.5)
            h.GetXaxis().SetLabelSize(19)

        set_mc_style_marker(g_exot3              , kBlue-2   , shape = 25 , line_width = 2)
        set_mc_style_marker(g_presel             , kRed-2    , shape = 23 , line_width = 2)
        set_mc_style_marker(g_dyjj               , kYellow+2 , shape = 24 , line_width = 2)
        set_mc_style_marker(g_topo               , kBlue+2   , shape = 20 , line_width = 2)
        set_mc_style_marker(g_topo_bosontag      , kGreen+2  , shape = 21 , line_width = 2)
        set_mc_style_marker(g_topo_bosontag_ntrk , kRed+2    , shape = 22 , line_width = 2)

        # SET UP THE CANVAS
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.1, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.10)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        self.pad1.SetGridy(1)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad1.cd()

        g_exot3.Draw("APCE")
        g_presel.Draw("PCE,same")
        g_dyjj.Draw("PCE,same")
        g_topo.Draw("PCE,same")
        g_topo_bosontag_ntrk.Draw("PCE,same")
        g_topo_bosontag.Draw("PCE,same")

        self.pad1.RedrawAxis("g")

        self.canvas.cd()
        self.leg.AddEntry(g_exot3              , "EXOT3 Derivation")
        self.leg.AddEntry(g_presel             , "Preselection")
        self.leg.AddEntry(g_dyjj               , "|#Deltay|")
        self.leg.AddEntry(g_topo               , "p_{T} asymmetry")
        self.leg.AddEntry(g_topo_bosontag      , "Boson tag")
        self.leg.AddEntry(g_topo_bosontag_ntrk , "n_{trk}")
        self.leg.Draw()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".pdf")
        self.canvas.Clear()

class PlotMjjQCDEfficiency(PlotBase):
    def __init__(self, signal_region, flip_legend = False, **kwargs):

        super(PlotMjjQCDEfficiency, self).__init__(
                width = 600,
                tex_size_mod = 1.4,
                hide_lumi = True,
                name = "QCD_Eff_" + signal_region,
                atlas_mod = "Simulation",
                extra_legend_lines = ["Internal"],
                legend_loc = [0.20,0.93,0.56,0.55] if flip_legend else [0.62,0.90,0.90,0.70],
                atlas_loc = [0.6,0.9] if flip_legend else [0.2,0.85],
                extra_lines_loc = [0.6,0.8] if flip_legend else [0.2,0.73],
                x_title = "m_{JJ}",
                x_units = "GeV",
                x_min = 1000,
                x_max = 6100,
                **kwargs)

        # GET PLOTS FROM FILE

        if "WW" in signal_region:
            bosontag_str = "WWNoNtrk"
            bosontag_ntrk_str = "SRWW"
        elif "ZZ" in signal_region:
            bosontag_str = "ZZNoNtrk"
            bosontag_ntrk_str = "SRZZ"
        elif "WZ" in signal_region:
            bosontag_str = "WZNoNtrk"
            bosontag_ntrk_str = "SRWZ"

        histos                       = {}
        histos["exot3"]              = RAW.get_hist(["qcd", "nominal"] , "h_mjj_noSelection")
        histos["presel"]             = RAW.get_hist(["qcd", "nominal"] , "h_mjj_noTopo")
        histos["dyjj"]               = RAW.get_hist(["qcd", "nominal"] , "h_mjj_TopoNoptasym")
        histos["topo"]               = RAW.get_hist(["qcd", "nominal"] , "h_mjj_Topo")
        histos["topo_bosontag"]      = RAW.get_hist(["qcd", "nominal"] , "h_mjj_" + bosontag_str)
        histos["topo_bosontag_ntrk"] = RAW.get_hist(["qcd", "nominal"] , "h_mjj_" + bosontag_ntrk_str)

        g_presel             = TGraphAsymmErrors()
        g_dyjj               = TGraphAsymmErrors()
        g_topo               = TGraphAsymmErrors()
        g_topo_bosontag      = TGraphAsymmErrors()
        g_topo_bosontag_ntrk = TGraphAsymmErrors()

        g_presel.Divide(histos["presel"], histos["exot3"], "n")
        g_dyjj.Divide(histos["dyjj"], histos["exot3"], "n")
        g_topo.Divide(histos["topo"], histos["exot3"], "n")
        g_topo_bosontag.Divide(histos["topo_bosontag"], histos["exot3"], "n")
        g_topo_bosontag_ntrk.Divide(histos["topo_bosontag_ntrk"], histos["exot3"], "n")

        all_graphs = [g_presel, g_dyjj, g_topo, g_topo_bosontag, g_topo_bosontag_ntrk]

        for h in all_graphs:
            h.GetHistogram().SetMinimum(self.y_min)
            h.GetHistogram().SetMaximum(self.y_max)
            if ("WZ" in signal_region):
                h.GetYaxis().SetTitle("#epsilon_{WZ}")
            elif ("WW" in signal_region):
                h.GetYaxis().SetTitle("#epsilon_{WW}")
            elif ("ZZ" in signal_region):
                h.GetYaxis().SetTitle("#epsilon_{ZZ}")
            else:
                print "invalid signal type"
                sys.exit(1)

            h.GetXaxis().SetLabelSize(0)
            h.GetYaxis().SetTitleOffset(2.0)
            h.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            h.GetXaxis().SetRangeUser(self.x_min, self.x_max)
            h.GetXaxis().SetTitleOffset(1.5)
            h.GetXaxis().SetLabelSize(19)

        set_mc_style_marker(g_presel             , kRed-2    , shape = 23 , line_width = 2)
        set_mc_style_marker(g_dyjj               , kYellow+2 , shape = 24 , line_width = 2)
        set_mc_style_marker(g_topo               , kBlue+2   , shape = 20 , line_width = 2)
        set_mc_style_marker(g_topo_bosontag      , kGreen+2  , shape = 21 , line_width = 2)
        set_mc_style_marker(g_topo_bosontag_ntrk , kRed+2    , shape = 22 , line_width = 2)

        # SET UP THE CANVAS
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.1, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.10)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        self.pad1.SetGridy(1)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad1.cd()

        g_presel.Draw("APCE")
        g_dyjj.Draw("PCE,same")
        g_topo.Draw("PCE,same")
        g_topo_bosontag_ntrk.Draw("PCE,same")
        g_topo_bosontag.Draw("PCE,same")

        self.pad1.RedrawAxis("g")

        self.canvas.cd()
        self.leg.AddEntry(g_presel             , "Preselection")
        self.leg.AddEntry(g_dyjj               , "|#Deltay|")
        self.leg.AddEntry(g_topo               , "p_{T} asymmetry")
        self.leg.AddEntry(g_topo_bosontag      , "Boson tag")
        self.leg.AddEntry(g_topo_bosontag_ntrk , "n_{trk}")
        self.leg.Draw()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".pdf")
        self.canvas.Clear()
# {{{
for selection in ["noTopo", "Topo", "WZNoNtrk", "TopoNoptasym", "WZNoD2", "WZNoMass"]:
    selection_str = selection
    if (selection == "noTopo"):
        selection_str = "Baseline Selection"
    elif (selection == "Topo"):
        selection_str = "Topological Selection"
    elif (selection == "WZNoNtrk"):
        selection_str = "WZ, without n_{trk}"
    elif (selection == "WZNoD2"):
        selection_str = "WZ, without D_{2}"
    elif (selection == "WZNoMass"):
        selection_str = "WZ, without m^{comb}"
    elif (selection == "TopoNoptasym"):
        selection_str = "Baseline + |#Delta y| Cut"

    default_lines =  ["HVT W' #rightarrow WZ", selection_str]

    if MAKE_DATAMC_PLOTS:
        PlotSimpleDataMC("h_mjj_" + selection,
                "HVT_Agv1_VcWZ_qqqq_m2000",
                empty_scale = 3.5,
                x_min = 1100,
                y_min = 0.11,
                log_scale = True,
                extra_legend_lines = default_lines
                )

    for signal_mass in ["1500","2000","2400","3000","4000","5000"]:
        if MAKE_SOVERB_PLOTS:
            PlotSoverB( "h_dyjjEffNum_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines,
                    x_min = 0,
                    x_max = 4 if (selection == "noTopo") else 1.2,
                    )

            PlotSoverB( "h_ptasymEffNum_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines,
                    x_min = 0,
                    x_max = 0.4 if (selection == "noTopo" or selection == "TopoNoptasym") else 0.15,
                    )

            PlotSoverB( "h_ntrkEffNum_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines,
                    x_min = 0,
                    x_max = 80,
                    )

        if MAKE_DATAMC_PLOTS:
            for ichep_str in ["PreICHEP", "PostICHEP", ""]:
                PlotSimpleDataMC("h_lead_jet_ntrk_" + selection + ichep_str + "Mjj10percWind" + signal_mass,
                        "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                        empty_scale = 1.5,
                        x_max = 80,
                        rebin = 2,
                        extra_legend_lines = default_lines + [ichep_str]
                        )

                PlotSimpleDataMC("h_sub_jet_ntrk_" + selection + ichep_str + "Mjj10percWind" + signal_mass,
                        "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                        empty_scale = 1.5,
                        x_max = 80,
                        rebin = 2,
                        extra_legend_lines = default_lines + [ichep_str]
                        )

                PlotSimpleDataMC("h_first_jet_ntrk_" + selection + ichep_str + "Mjj10percWind" + signal_mass,
                        "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                        empty_scale = 1.5,
                        x_max = 80,
                        rebin = 2,
                        extra_legend_lines = default_lines + [ichep_str]
                        )

                PlotSimpleDataMC("h_second_jet_ntrk_" + selection + ichep_str + "Mjj10percWind" + signal_mass,
                        "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                        empty_scale = 1.5,
                        x_max = 80,
                        rebin = 2,
                        extra_legend_lines = default_lines + [ichep_str]
                        )

            PlotSimpleDataMC("h_dyjj_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.7,
                    x_max = 4 if (selection == "noTopo") else 1.2,
                    rebin = 4 if (selection == "noTopo") else 2,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_ptasym_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines,
                    x_max = 0.4 if (selection == "noTopo" or selection == "TopoNoptasym") else 0.15,
                    )


            PlotSimpleDataMC("h_lead_jet_pt_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_max = 3500,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_sub_jet_pt_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_max = 3500,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_first_jet_pt_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_max = 3500,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_second_jet_pt_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_max = 3500,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_lead_jet_m_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_min = 50,
                    x_max = 150,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_sub_jet_m_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_min = 50,
                    x_max = 150,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_first_jet_m_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_min = 50,
                    x_max = 150,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_second_jet_m_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    x_min = 50,
                    x_max = 150,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_lead_jet_D2_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_sub_jet_D2_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_first_jet_D2_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_second_jet_D2_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_lead_jet_phi_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    rebin = 2,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_sub_jet_phi_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    rebin = 2,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_first_jet_phi_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    rebin = 2,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_second_jet_phi_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    rebin = 2,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_lead_jet_eta_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_sub_jet_eta_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_first_jet_eta_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

            PlotSimpleDataMC("h_second_jet_eta_" + selection + "Mjj10percWind" + signal_mass,
                    "HVT_Agv1_VcWZ_qqqq_m" + signal_mass,
                    empty_scale = 1.5,
                    extra_legend_lines = default_lines
                    )

# }}}

if MAKE_EFF_PLOTS:
    for sample in SIGNAL_SAMPLES:
        PlotMjjSignalEfficiency(sample)
    PlotMjjQCDEfficiency("WW", log_scale = True, y_min = 1e-7, y_max = 1e2)
    PlotMjjQCDEfficiency("WZ", log_scale = True, y_min = 1e-7, y_max = 1e2)
    PlotMjjQCDEfficiency("ZZ", log_scale = True, y_min = 1e-7, y_max = 1e2)
    PlotMjjQCDEfficiency("WW", y_min = 1e-8, y_max = 1e-4)
    PlotMjjQCDEfficiency("WZ", y_min = 1e-8, y_max = 1e-4)
    PlotMjjQCDEfficiency("ZZ", y_min = 1e-8, y_max = 1e-4)
