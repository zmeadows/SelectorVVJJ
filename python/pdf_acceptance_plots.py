from ROOT import *
import atlas_style

import os
import math
from sys import argv, exit

from plot_base import *
from plot_util import *
from plot_loader import *
from plot_vvjj import *

gROOT.SetBatch()
sane_defaults()
TGaxis.SetMaxDigits(4)
gStyle.SetOptStat(0)

RAW = PlotLoader("/afs/cern.ch/work/z/zmeadows/public/VVJJ/LocalTest/pdf_acceptance_03032017/processed_ntuples/cp.merged.pdfbranches.absY.root")
ROOT_OUTPUT_DIR = "/afs/cern.ch/work/z/zmeadows/public/VVJJ/LocalTest/pdf_acceptance_03032017/processed_ntuples/plots"

OUTPUT_DIR = ROOT_OUTPUT_DIR
make_dir(ROOT_OUTPUT_DIR)
make_dir(OUTPUT_DIR)


SIGNAL_SAMPLES = ["HVT_Agv1_VcWZ_qqqq", "HVT_Agv1_VzWW_qqqq", "RS_G_WW_qqqq_c10", "RS_G_ZZ_qqqq_c10"]

SIGNAL_MASS_POINTS = [
    "1100", "1200", "1300", "1400", "1500",
    "1600", "1700", "1800", "1900", "2000", "2200", "2400",
    "2600", "2800", "3000", "3500", "4000", "4500", "5000"
    ]

# {{{
class PlotNominalPDFComparison(PlotBase):
    def __init__(self, signal_name, var_name, **kwargs):

        super(PlotNominalPDFComparison, self).__init__(
                hide_lumi = True,
                width = 600,
                name = "PDFupdown_comparison_" + signal_name + "_" + var_name,
                atlas_mod = "Sim. Internal",
                atlas_loc = [0.2,0.9],
                extra_lines_loc = [0.2,0.81],
                x_title = "Dijet Mass m_{JJ}",
                x_units = "GeV",
                legend_loc = [0.65,0.94,0.88,0.76],
                **kwargs)

        # GET PLOTS FROM FILE

        h_nominal  = RAW.get_hist([signal_name, "nominal"]          , var_name)
        h_pdf_up   = RAW.get_hist([signal_name, "ATLAS_PDF_1up" ]   , var_name)
        h_pdf_down = RAW.get_hist([signal_name, "ATLAS_PDF_1down" ] , var_name)

        self.determine_y_axis_title(h_nominal)

        all_hists = [h_nominal, h_pdf_up, h_pdf_down]
        for h in all_hists:
            h.GetYaxis().SetTitle(self.y_title)
            h.GetXaxis().SetTitle("")
            h.GetXaxis().SetLabelSize(0)
            h.GetYaxis().SetTitleOffset(2.0)
            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)

        self.pad_empty_space(all_hists)

        if self.log_scale: self.name += "_log"

        set_mc_style_line(h_nominal, kBlack, line_width = 2)
        set_mc_style_line(h_pdf_up, kRed, line_width = 2)
        set_mc_style_line(h_pdf_down, kBlue, line_width = 2)

        h_ratio_up = h_pdf_up.Clone("h_ratio_up")
        h_ratio_up.Divide(h_ratio_up, h_nominal, 1, 1, "")
        h_ratio_down = h_pdf_down.Clone("h_ratio_down")
        h_ratio_down.Divide(h_ratio_down, h_nominal, 1, 1, "")

        ratio_title = "PDF/Nominal"
        h_ratio_up.GetYaxis().SetTitle(ratio_title)
        h_ratio_down.GetYaxis().SetTitle(ratio_title)

        h_ratio_up.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
        h_ratio_down.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)

        h_ratio_up.GetYaxis().SetTitleOffset(2.0)
        h_ratio_up.GetXaxis().SetTitleOffset(4.0)
        h_ratio_up.GetXaxis().SetLabelSize(19)
        h_ratio_down.GetYaxis().SetTitleOffset(2.0)
        h_ratio_down.GetXaxis().SetTitleOffset(4.0)
        h_ratio_down.GetXaxis().SetLabelSize(19)

        set_mc_style_marker(h_ratio_up, kRed)
        set_mc_style_marker(h_ratio_down, kBlue)

        h_ratio_down.SetMinimum(0.001)
        h_ratio_down.SetMaximum(1.999)
        h_ratio_up.SetMinimum(0.001)
        h_ratio_up.SetMaximum(1.999)

        h_ratio_down.GetYaxis().SetNdivisions(504);
        h_ratio_up.GetYaxis().SetNdivisions(504);

        # SET UP THE CANVAS
        self.canvas.Divide(1,2)
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.33, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.00)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad2 = self.canvas.cd(2)
        self.pad2.SetPad(0.0, 0.0, 1., 0.33)
        self.pad2.SetTopMargin(0.05)
        self.pad2.SetRightMargin(0.07)
        self.pad2.SetBottomMargin(0.3)
        self.pad2.SetFillColorAlpha(0, 0.)
        self.pad2.SetGridy(1) # grid for ratio plot
        self.pad2.SetBorderSize(0)

        self.pad1.cd()

        h_nominal.Draw("hist")
        h_pdf_up.Draw("hist,same")
        h_pdf_down.Draw("hist,same")

        self.pad1.RedrawAxis("g")

        self.canvas.cd()
        self.leg.AddEntry(h_nominal, "Nominal")
        self.leg.AddEntry(h_pdf_up, "1 #sigma PDF Up")
        self.leg.AddEntry(h_pdf_down, "1 #sigma PDF Down")
        self.leg.Draw()

        self.pad2.cd()

        h_ratio_up.Draw("APE,same")
        h_ratio_down.Draw("PE,same")
        self.pad2.RedrawAxis("g")
        self.pad1.RedrawAxis()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".pdf")
        self.canvas.Clear()
# }}}

# {{{
class PlotPDFAcceptance(PlotBase):
    def __init__(self, signal_name, mass_point, **kwargs):

        mass_point_str = "m" + str(mass_point)

        super(PlotPDFAcceptance, self).__init__(
                hide_lumi = True,
                width = 600,
                name = "mjj_PDFacceptanceRatio_" + signal_name + "_" + mass_point_str,
                atlas_mod = "Sim. Internal",
                atlas_loc = [0.2,0.85],
                extra_lines_loc = [0.2,0.75],
                x_title = "Dijet Mass m_{JJ}",
                x_units = "GeV",
                **kwargs)

        # GET PLOTS FROM FILE

        if "WZ" in signal_name:
            sig_var_name = "h_mjj_SRWZ"
        elif "WW" in signal_name:
            sig_var_name = "h_mjj_SRWW"
        elif "ZZ" in signal_name:
            sig_var_name = "h_mjj_SRZZ"

        h_nominal_nosel  = RAW.get_hist([signal_name + "_" + mass_point_str, "nominal"]          , "h_mjj_noSelection")
        h_nominal_WZ     = RAW.get_hist([signal_name + "_" + mass_point_str, "nominal"]          , sig_var_name)
        h_pdf_nosel_up   = RAW.get_hist([signal_name + "_" + mass_point_str, "ATLAS_PDF_1up" ]   , "h_mjj_noSelection")
        h_pdf_WZ_up      = RAW.get_hist([signal_name + "_" + mass_point_str, "ATLAS_PDF_1up" ]   , sig_var_name)
        h_pdf_nosel_down = RAW.get_hist([signal_name + "_" + mass_point_str, "ATLAS_PDF_1down" ] , "h_mjj_noSelection")
        h_pdf_WZ_down    = RAW.get_hist([signal_name + "_" + mass_point_str, "ATLAS_PDF_1down" ] , sig_var_name)

        h_acc_pdf_up = h_pdf_WZ_up.Clone("h_acc_pdf_up")
        h_acc_pdf_up.Divide(h_acc_pdf_up, h_pdf_nosel_up, 1, 1, "B")

        h_acc_pdf_down = h_pdf_WZ_down.Clone("h_acc_pdf_down")
        h_acc_pdf_down.Divide(h_acc_pdf_down, h_pdf_nosel_down, 1, 1, "B")

        h_acc_nom = h_nominal_WZ.Clone("h_acc_nom")
        h_acc_nom.Divide(h_acc_nom, h_nominal_nosel, 1, 1, "B")

        h_fratio_up = h_acc_pdf_up.Clone("h_fratio_up")
        h_fratio_up.Divide(h_fratio_up, h_acc_nom, 1, 1, "B")

        h_fratio_down = h_acc_pdf_down.Clone("h_fratio_down")
        h_fratio_down.Divide(h_fratio_down, h_acc_nom, 1, 1, "B")

        if (self.rebin != None):
            h_fratio_up.Rebin(self.rebin)
            h_fratio_down.Rebin(self.rebin)

        h_fratio_up.GetYaxis().SetTitle("PDF Acceptance Ratio")
        h_fratio_up.GetYaxis().SetTitleSize(25)
        h_fratio_up.GetYaxis().SetTitleOffset(1.5)
        h_fratio_down.GetYaxis().SetTitle("PDF/Nom. Acceptance Ratio")
        h_fratio_down.GetYaxis().SetTitleSize(25)
        h_fratio_down.GetYaxis().SetTitleOffset(1.5)

        f_up = TF1("f_up","pol0",self.x_min,self.x_max)
        f_down = TF1("f_down","pol0",self.x_min,self.x_max)

        fres_up = h_fratio_up.Fit(f_up,"SQN")
        fres_down = h_fratio_down.Fit(f_down,"SQN")

        quad = sqrt( abs(fres_down.Parameter(0)-1.0)**2 + abs(fres_up.Parameter(0) - 1.0)**2 )
        sum = abs(fres_down.Parameter(0) - 1.0) + abs(fres_up.Parameter(0) - 1.0)

        self.extra_legend_lines += [
                "UP: " +  str( round( (fres_up.Parameter(0) - 1.0)*100., 2 ) ) + " %"
                + " DOWN: " + str( round( (fres_down.Parameter(0) - 1.0)*100., 2 ) ) + " %"
                + " SUM: " + str( round( sum*100., 2 ) ) + " %"]

        #print fres_up.Parameter(0), fres_up.ParError(0)
        #print fres_down.Parameter(0), fres_down.ParError(0)

        f_up.SetLineColorAlpha(kRed, 0.8)
        f_up.SetLineWidth(2)
        f_down.SetLineColorAlpha(kBlue,0.8)
        f_down.SetLineWidth(2)

        self.set_x_axis_bounds(h_fratio_up)
        self.set_y_min(h_fratio_up)
        self.set_y_max(h_fratio_up)
        self.set_x_axis_bounds(h_fratio_down)
        self.set_y_min(h_fratio_down)
        self.set_y_max(h_fratio_down)

        if self.log_scale: self.name += "_log"

        set_mc_style_marker(h_fratio_up, kRed, shape = 22, line_style = 4, line_width = 1, line_alpha = 0.7)
        set_mc_style_marker(h_fratio_down, kBlue, shape = 23, line_style = 5, line_width = 1, line_alpha = 0.7)

        self.canvas.Modified()

        # SET UP THE CANVAS
        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.1, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.10)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        self.pad1.SetGridy(1)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad1.cd()

        h_fratio_up.Draw("PE")
        h_fratio_down.Draw("PE,same")
        f_up.Draw("same")
        f_down.Draw("same")

        self.pad1.RedrawAxis("g")


        self.canvas.Update()
        self.canvas.Modified()

        self._make_decorations()

        self.canvas.cd()
        self.leg.AddEntry(h_fratio_up, "1 #sigma PDF Up")
        self.leg.AddEntry(h_fratio_down, "1 #sigma PDF Down")
        self.leg.Draw()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".png")
        self.canvas.Clear()

# }}}

# {{{
class PlotPdfAcceptanceRatios(PlotBase):
    def __init__(self, flip_legend = False, **kwargs):

        super(PlotPdfAcceptanceRatios, self).__init__(
                width = 600,
                tex_size_mod = 1.4,
                hide_lumi = True,
                name = "PDF_Signal_Acceptance",
                atlas_mod = "Simulation",
                extra_legend_lines = ["Internal"],
                legend_loc = [0.20,0.93,0.56,0.55] if flip_legend else [0.58,0.90,0.90,0.65],
                atlas_loc = [0.6,0.9] if flip_legend else [0.2,0.85],
                extra_lines_loc = [0.6,0.8] if flip_legend else [0.2,0.73],
                x_title = "Resonance Mass",
                x_units = "GeV",
                x_min = 1000,
                x_max = 5100,
                **kwargs)

        # GET PLOTS FROM FILE

        graphs_up = {}
        graphs_down = {}

        x_points = map(float, SIGNAL_MASS_POINTS)
        for sample in SIGNAL_SAMPLES:
            y_points_up = []
            y_points_down = []
            for mass_point in SIGNAL_MASS_POINTS:
                full_signal_name = sample + "_m" + mass_point

                if "WW" in sample:
                    sig_var_name = "h_mjj_SRWW"
                elif "ZZ" in sample:
                    sig_var_name = "h_mjj_SRZZ"
                elif "WZ" in sample:
                    sig_var_name = "h_mjj_SRWZ"
                else:
                    print "WARNING: WW/WZ/ZZ not found in signal sample name"
                    sys.exit(1)

                count_nominal_nosel  = float(RAW.get_hist([full_signal_name, "nominal"]          , "h_mjj_noSelection").Integral())
                count_nominal_sig    = float(RAW.get_hist([full_signal_name, "nominal"]          , sig_var_name).Integral())
                count_pdf_nosel_up   = float(RAW.get_hist([full_signal_name, "ATLAS_PDF_1up" ]   , "h_mjj_noSelection").Integral())
                count_pdf_sig_up     = float(RAW.get_hist([full_signal_name, "ATLAS_PDF_1up" ]   , sig_var_name).Integral())
                count_pdf_nosel_down = float(RAW.get_hist([full_signal_name, "ATLAS_PDF_1down" ] , "h_mjj_noSelection").Integral())
                count_pdf_sig_down   = float(RAW.get_hist([full_signal_name, "ATLAS_PDF_1down" ] , sig_var_name).Integral())

                acc_nominal = count_nominal_sig/count_nominal_nosel
                acc_up      = count_pdf_sig_up/count_pdf_nosel_up
                acc_down    = count_pdf_sig_down/count_pdf_nosel_down

                acc_ratio_up = acc_up / acc_nominal
                acc_ratio_down = acc_down / acc_nominal


                y_points_up.append(acc_ratio_up)
                y_points_down.append(acc_ratio_down)

            assert(len(x_points) == len(y_points_up))
            assert(len(x_points) == len(y_points_down))

            arr_x_points      = array.array('f', x_points)
            arr_y_points_up   = array.array('f', y_points_up)
            arr_y_points_down = array.array('f', y_points_down)

            g_up  = TGraph(len(x_points), arr_x_points, arr_y_points_up)
            g_down = TGraph(len(x_points), arr_x_points, arr_y_points_down)

            graphs_up[full_signal_name]   = g_up
            graphs_down[full_signal_name] = g_down

        color_list = [2,4,6,8]
        shape_list = [20,21,22,23]
        i = 0
        for name, g in graphs_up.iteritems():
            g.GetHistogram().SetMinimum(0.901)
            g.GetHistogram().SetMaximum(1.3)
            g.GetYaxis().SetTitle("PDF Acceptance Ratio")
            g.GetXaxis().SetLabelSize(0)
            g.GetYaxis().SetTitleOffset(2.0)
            g.GetXaxis().SetRangeUser(self.x_min, self.x_max)
            g.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            g.GetXaxis().SetTitleOffset(1.5)
            g.GetXaxis().SetLabelSize(19)
            set_mc_style_marker(g , color_list[i] , shape = shape_list[i] , line_width = 0)
            i = i + 1

        color_list = [2,4,6,8]
        shape_list = [24,25,26,32]
        i = 0
        for name, g in graphs_down.iteritems():
            g.GetHistogram().SetMinimum(0.901)
            g.GetHistogram().SetMaximum(1.3)
            g.GetYaxis().SetTitle("PDF Acceptance Ratio")
            g.GetXaxis().SetLabelSize(0)
            g.GetYaxis().SetTitleOffset(2.0)
            g.GetXaxis().SetRangeUser(self.x_min, self.x_max)
            g.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            g.GetXaxis().SetTitleOffset(1.5)
            g.GetXaxis().SetLabelSize(19)
            set_mc_style_marker(g , color_list[i] , shape = shape_list[i] , line_width = 0)
            i = i + 1

        # SET UP THE CANVAS
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.1, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.10)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        self.pad1.SetGridy(1)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad1.cd()

        i = 0
        for name,g in graphs_up.iteritems():
            if (i == 0):
                g.Draw("APCE")
            else:
                g.Draw("PCE,same")
            i = i + 1

        for name,g in graphs_down.iteritems():
            g.Draw("PCE,same")


        self.pad1.RedrawAxis("g")

        self.canvas.cd()

        for name,g in graphs_up.iteritems():
            if ("HVT" in name and "WZ" in name):
                title_str = "HVT W' #rightarrow WZ, PDF Up"
            if ("HVT" in name and "WW" in name):
                title_str = "HVT W' #rightarrow WW, PDF Up"
            if ("RS" in name and "WW" in name):
                title_str = "Bulk RS G #rightarrow WW, PDF Up"
            if ("RS" in name and "ZZ" in name):
                title_str = "Bulk RS G #rightarrow ZZ, PDF Up"
            self.leg.AddEntry(g, title_str)

        for name,g in graphs_down.iteritems():
            if ("HVT" in name and "WZ" in name):
                title_str = "HVT W' #rightarrow WZ, PDF Down"
            if ("HVT" in name and "WW" in name):
                title_str = "HVT W' #rightarrow WW, PDF Down"
            if ("RS" in name and "WW" in name):
                title_str = "Bulk RS G #rightarrow WW, PDF Down"
            if ("RS" in name and "ZZ" in name):
                title_str = "Bulk RS G #rightarrow ZZ, PDF Down"
            self.leg.AddEntry(g, title_str)

        self.leg.Draw()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".png")
        self.canvas.Clear()
# }}}

class PlotMeanRapidity(PlotBase):
    def __init__(self, noselection, flip_legend = False, **kwargs):

        super(PlotMeanRapidity, self).__init__(
                width = 600,
                tex_size_mod = 1.4,
                hide_lumi = True,
                name = "PDF_MeanAbsY_NoSelection" if noselection else "PDF_MeanAbsY_FullSignalSelection",
                atlas_mod = "Simulation",
                extra_legend_lines = ["Internal"],
                legend_loc = [0.20,0.93,0.56,0.55] if flip_legend else [0.58,0.90,0.90,0.65],
                atlas_loc = [0.6,0.9] if flip_legend else [0.2,0.85],
                extra_lines_loc = [0.6,0.8] if flip_legend else [0.2,0.73],
                x_title = "Resonance Mass",
                x_units = "GeV",
                x_min = 1000,
                x_max = 5100,
                **kwargs)

        # GET PLOTS FROM FILE

        graphs_up = {}
        graphs_down = {}

        x_points = map(float, SIGNAL_MASS_POINTS)
        for sample in SIGNAL_SAMPLES:
            y_points_up = []
            y_points_down = []
            for mass_point in SIGNAL_MASS_POINTS:
                full_signal_name = sample + "_m" + mass_point

                if noselection:
                    var_name = "h_meanY_noSelection"
                else:
                    if "WW" in sample:
                        var_name = "h_meanY_SRWW"
                    elif "ZZ" in sample:
                        var_name = "h_meanY_SRZZ"
                    elif "WZ" in sample:
                        var_name = "h_meanY_SRWZ"
                    else:
                        print "WARNING: WW/WZ/ZZ not found in signal sample name"
                        sys.exit(1)

                avg_pdf_up   = float(RAW.get_hist([full_signal_name, "ATLAS_PDF_1up" ]   , var_name).GetMean())
                avg_pdf_down = float(RAW.get_hist([full_signal_name, "ATLAS_PDF_1up" ]   , var_name).GetMean())

                y_points_up.append(avg_pdf_up)
                y_points_down.append(avg_pdf_down)

            assert(len(x_points) == len(y_points_up))
            assert(len(x_points) == len(y_points_down))

            arr_x_points      = array.array('f', x_points)
            arr_y_points_up   = array.array('f', y_points_up)
            arr_y_points_down = array.array('f', y_points_down)

            g_up  = TGraph(len(x_points), arr_x_points, arr_y_points_up)
            g_down = TGraph(len(x_points), arr_x_points, arr_y_points_down)

            graphs_up[full_signal_name]   = g_up
            graphs_down[full_signal_name] = g_down

        color_list = [2,4,6,8]
        shape_list = [20,21,22,23]
        i = 0
        for name, g in graphs_up.iteritems():
            g.GetHistogram().SetMinimum(0.2)
            g.GetHistogram().SetMaximum(1.0)
            g.GetYaxis().SetTitle("#frac{1}{2}(|y_{1}| + |y_{2}|)")
            g.GetXaxis().SetLabelSize(0)
            g.GetYaxis().SetTitleOffset(2.0)
            g.GetXaxis().SetRangeUser(self.x_min, self.x_max)
            g.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            g.GetXaxis().SetTitleOffset(1.5)
            g.GetXaxis().SetLabelSize(19)
            set_mc_style_marker(g , color_list[i] , shape = shape_list[i] , line_width = 0)
            i = i + 1

        color_list = [2,4,6,8]
        shape_list = [24,25,26,32]
        i = 0
        for name, g in graphs_down.iteritems():
            g.GetHistogram().SetMinimum(0.2)
            g.GetHistogram().SetMaximum(1.0)
            g.GetYaxis().SetTitle("#frac{1}{2}(|y_{1}| + |y_{2}|)")
            g.GetXaxis().SetLabelSize(0)
            g.GetYaxis().SetTitleOffset(2.0)
            g.GetXaxis().SetRangeUser(self.x_min, self.x_max)
            g.GetXaxis().SetTitle(self.x_title + " " + self.x_units_str)
            g.GetXaxis().SetTitleOffset(1.5)
            g.GetXaxis().SetLabelSize(19)
            set_mc_style_marker(g , color_list[i] , shape = shape_list[i] , line_width = 0)
            i = i + 1

        # SET UP THE CANVAS
        self.canvas.Modified()

        self.pad1 = self.canvas.cd(1)
        self.pad1.SetPad(0.0, 0.1, 1., 1.)
        self.pad1.SetTopMargin(0.07)
        self.pad1.SetRightMargin(0.07)
        self.pad1.SetBottomMargin(0.10)
        self.pad1.SetFillColorAlpha(0, 0.)
        self.pad1.SetBorderSize(0)
        self.pad1.SetGridy(1)
        self.pad1.SetBorderSize(0)
        if self.log_scale: self.pad1.SetLogy()

        self.pad1.cd()

        i = 0
        for name,g in graphs_up.iteritems():
            if (i == 0):
                g.Draw("APCE")
            else:
                g.Draw("PCE,same")
            i = i + 1

        for name,g in graphs_down.iteritems():
            g.Draw("PCE,same")


        self.pad1.RedrawAxis("g")

        self.canvas.cd()

        for name,g in graphs_up.iteritems():
            if ("HVT" in name and "WZ" in name):
                title_str = "HVT W' #rightarrow WZ, PDF Up"
            if ("HVT" in name and "WW" in name):
                title_str = "HVT W' #rightarrow WW, PDF Up"
            if ("RS" in name and "WW" in name):
                title_str = "Bulk RS G #rightarrow WW, PDF Up"
            if ("RS" in name and "ZZ" in name):
                title_str = "Bulk RS G #rightarrow ZZ, PDF Up"
            self.leg.AddEntry(g, title_str)

        for name,g in graphs_down.iteritems():
            if ("HVT" in name and "WZ" in name):
                title_str = "HVT W' #rightarrow WZ, PDF Down"
            if ("HVT" in name and "WW" in name):
                title_str = "HVT W' #rightarrow WW, PDF Down"
            if ("RS" in name and "WW" in name):
                title_str = "Bulk RS G #rightarrow WW, PDF Down"
            if ("RS" in name and "ZZ" in name):
                title_str = "Bulk RS G #rightarrow ZZ, PDF Down"
            self.leg.AddEntry(g, title_str)

        self.leg.Draw()

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(OUTPUT_DIR + "/" + self.name + ".png")
        self.canvas.Clear()

### MAKE PLOTS ###

PlotMeanRapidity(True)
PlotMeanRapidity(False)

# PlotPdfAcceptanceRatios()
# 
# for signal_name in SIGNAL_SAMPLES:
#     if ("HVT" in signal_name and "WZ" in signal_name):
#         sd = "HVT W^' #rightarrow WZ"
#     if ("HVT" in signal_name and "WW" in signal_name):
#         sd = "HVT W^' #rightarrow WW"
#     if ("RS" in signal_name and "WW" in signal_name):
#         sd = "Bulk RS G #rightarrow WW"
#     if ("RS" in signal_name and "ZZ" in signal_name):
#         sd = "Bulk RS G #rightarrow ZZ"
#     for mass_point in SIGNAL_MASS_POINTS:
#         PlotPDFAcceptance(
#                 signal_name,
#                 int(mass_point),
#                 x_min = 1100,
#                 x_max = min(6000, int(mass_point) + 1500),
#                 y_min = 0.5,
#                 y_max = 2,
#                 extra_legend_lines = [ sd, "m = " + str(round(int(mass_point)/1000.,2)) + " TeV" ]
#                 )
#         PlotPDFAcceptance(
#                 signal_name,
#                 int(mass_point),
#                 y_min = 0.8,
#                 y_max = 1.4,
#                 x_min = 1100,
#                 x_max = min(6000, int(mass_point) + 1500),
#                 extra_legend_lines = [ sd, "m = " + str(round(int(mass_point)/1000.,2)) + " TeV" ]
#                 )
# 
# 
# for signal_name in SIGNAL_SAMPLES:
#     for mass_point in SIGNAL_MASS_POINTS:
# 
#         if "WZ" in signal_name:
#             sr = "SRWZ"
#         elif "WW" in signal_name:
#             sr = "SRWW"
#         elif "ZZ" in signal_name:
#             sr = "SRZZ"
# 
#         if ("HVT" in signal_name and "WZ" in signal_name):
#             sd = "HVT W^{#prime} #rightarrow WZ"
#         if ("HVT" in signal_name and "WW" in signal_name):
#             sd = "HVT W^{#prime} #rightarrow WW"
#         if ("RS" in signal_name and "WW" in signal_name):
#             sd = "Bulk RS G #rightarrow WW"
#         if ("RS" in signal_name and "ZZ" in signal_name):
#             sd = "Bulk RS G #rightarrow ZZ"
# 
#         PlotNominalPDFComparison(
#                 signal_name + "_m" + mass_point,
#                 "h_mjj_noSelection",
#                 log_scale = True,
#                 x_min = 1100,
#                 x_max = 5500,
#                 extra_legend_lines = [ sd, sr + "Selection", "m = " + str(round(int(mass_point)/1000.,2)) + " TeV" ],
#                 empty_scale = 1.5
#                 )
# 
#         PlotNominalPDFComparison(
#                 signal_name + "_m" + mass_point,
#                 "h_mjj_" + sr,
#                 log_scale = True,
#                 x_min = 1100,
#                 x_max = 5500,
#                 extra_legend_lines = [ sd, sr + "Selection", "m = " + str(round(int(mass_point)/1000.,2)) + " TeV" ],
#                 empty_scale = 1.5
#                 )
# 
#         PlotNominalPDFComparison(
#                 signal_name + "_m" + mass_point,
#                 "h_meanY_noSelection",
#                 x_min = 0,
#                 x_max = 2,
#                 extra_legend_lines = [ sd, sr + "Selection", "m = " + str(round(int(mass_point)/1000.,2)) + " TeV" ],
#                 empty_scale = 1.5
#                 )
# 
#         PlotNominalPDFComparison(
#                 signal_name + "_m" + mass_point,
#                 "h_meanY_" + sr,
#                 x_min = 0,
#                 x_max = 2,
#                 extra_legend_lines = [ sd, sr + "Selection", "m = " + str(round(int(mass_point)/1000.,2)) + " TeV" ],
#                 empty_scale = 1.5
#                 )
