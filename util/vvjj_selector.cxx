#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <exception>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>

#include "SelectorVVJJ/VVJJSelector.h"
#include "SelectorVVJJ/util.h"

int main(int argc, char* argv[])
{
    std::string output_file;
    std::vector<std::string> input_files;
    std::string selection;
    std::vector<std::string> trees;
    std::string label;

    try
    {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help"          , "produce help message")
            ("output"        , po::value(&output_file)               , "output root file")
            ("input"         , po::value(&input_files)->multitoken() , "input root files (list separated by spaces)")
            ("selection"     , po::value(&selection)                 , "selection to use (see VVJJSelector.cxx/h)")
            ("trees"         , po::value(&trees)->multitoken()       , "trees in input file(s) to use (i.e. nominal         , JER__1up/down , etc)")
            ("label"         , po::value(&label)                     , "label for top-level folder in output file (ex: data , pythia_dijet  , etc)")
            ;

        po::variables_map vm;
        po::store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return EXIT_SUCCESS;
        }

        po::notify(vm);
    }
    catch(std::exception& e) {
        FATAL_ERROR(e.what());
    }


    if (output_file.empty())
        FATAL_ERROR("no output files specified.");

    if (input_files.empty())
        FATAL_ERROR("no input files specified.");

    if (selection.empty())
        WARNING("no selection criteria provided, proceeding to process all events.");

    if (trees.empty())
        FATAL_ERROR("no trees specified.");

    if (label.empty())
        label = trees[0];

    std::cout << "\nRUNNING VBF SELECTOR... \n" << std::endl;
    std::cout << "#### ARGUMENTS ####" << std::endl;
    std::cout << "output: " << output_file << std::endl;
    std::cout << "input: "; print_strings(input_files);
    std::cout << "selection: " << selection << std::endl;
    std::cout << "trees: "; print_strings(trees);
    std::cout << "label: " << label << std::endl;
    std::cout << "###################\n" << std::endl;

    // determine the names of all trees in the file if asked
    if (trees.size() == 1 && trees[0] == "all")
    {
        auto f_test = TFile::Open(input_files[0].c_str());
        trees = get_tree_names(f_test);
        f_test->Close();
        delete f_test;

        std::cout << "Found the following trees:" << std::endl;
        for (auto const& t_name : trees)
            std::cout << "\t" << t_name << std::endl;
    }

    // check that the specified input files exist and have the specified trees in them
    for (auto const& f_name : input_files) {
        auto f = TFile::Open(f_name.c_str());
        if (!f) FATAL_ERROR("input file: " + f_name + " could not be opened.");
        for (auto const& t_name : trees) {
            auto t = (TTree*) f->Get(t_name.c_str());
            if (!t) FATAL_ERROR("input file: " + f_name + " does not contain the requested tree: " + t_name);
        }
        f->Close();
        delete f;
    }


    std::ifstream outfile_test(output_file);
    if (outfile_test.good()) {
        std::cout << "WARNING: " << output_file << " already exists! exiting for safety..." << std::endl;
        return EXIT_SUCCESS;
    }

    auto f_out = TFile::Open(output_file.c_str(), "NEW");

    TSelector* vvjj_selector;

    for (auto const& t : trees) {
        auto tchain = new TChain(t.c_str());
        for (auto const& f_name : input_files)
            tchain->Add(f_name.c_str(), 0);

        vvjj_selector = new VVJJSelector(label, t, f_out);

        tchain->Process(vvjj_selector, TString(selection));
        delete vvjj_selector;
    }

    f_out->Close();
    delete f_out;
    return EXIT_SUCCESS;
}
