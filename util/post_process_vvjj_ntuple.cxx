#include <iostream>
#include <stdlib.h>
#include <exception>
#include <unordered_map>
#include <string>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>

#include "SelectorVVJJ/util.h"
#include "SelectorVVJJ/WeightTool.h"

// PDF uncertainty
#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/Reweighting.h"

int main(int argc, char* argv[])
{
    std::vector<std::string> input_files;
    double luminosity = -1.0;
    bool doPdfReweight = false;

    try
    {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help"           , "produce help message")
            ("input"          , po::value(&input_files)->multitoken() , "input root files (list separated by spaces)")
            ("luminosity"     , po::value<double>(&luminosity)        , "luminosity in 1/fb")
            ("addPDFbranches" , po::value<bool>(&doPdfReweight)       , "make pdf reweighting branches")
            ;

        po::variables_map vm;
        po::store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return EXIT_SUCCESS;
        }

        po::notify(vm);
    }
    catch(std::exception& e) {
        FATAL_ERROR(e.what());
    }

    if (input_files.empty())
        FATAL_ERROR("no input files specified.");

    if (luminosity < 0)
        FATAL_ERROR("no luminosity specified");

    std::cout << "\nPOST-PROCESSING VVJJ NTUPLES... \n" << std::endl;
    std::cout << "#### ARGUMENTS ####" << std::endl;
    std::cout << "input: "; print_strings(input_files);
    std::cout << "luminosity: " << luminosity << std::endl;
    std::cout << "doPdfReweight: " << doPdfReweight << std::endl;
    std::cout << "###################\n" << std::endl;

    auto weight_tool = new WeightTool();
    if (!weight_tool->read_weights_file("/afs/cern.ch/work/z/zmeadows/public/VVJJ/SelectorVVJJ/SelectorVVJJ/data/xsections.txt"))
        exit(1);

    std::pair<std::string, std::vector<LHAPDF::PDF*>> nominal_pdf_set;
    std::unordered_map<std::string, std::vector<LHAPDF::PDF*>> additional_pdf_sets;

    if (doPdfReweight)
    {
        const std::string nominal_pdf_set_name = "NNPDF23_lo_as_0119_qed";

        nominal_pdf_set = std::make_pair(nominal_pdf_set_name, LHAPDF::mkPDFs(nominal_pdf_set_name));

        for (auto const& pdf_name : {
                "CT14nlo",
                "MMHT2014nlo68cl",
                "ATLAS-epWZ12-EIG",
                "NNPDF30_nlo_as_0118"
                })
        {
            additional_pdf_sets.emplace(pdf_name, LHAPDF::mkPDFs(pdf_name));
        }
    }

    for (auto const& f_name : input_files) {
        auto f = TFile::Open(f_name.c_str(), "update");
        if (!f) {
            WARNING("input file: " + f_name + " could not be opened. Skipping...");
            f->Close();
            delete f;
            continue;
        }

        // compute the weights
        const std::vector<std::string> tree_names = get_tree_names(f);
        std::cout << "### " << f_name << " ###" << std::endl;
        std::cout << "processing the following branches:" << std::endl;
        for (auto const& b : tree_names)
            std::cout << "\t" << b << std::endl;

        for (auto const& tree_name : tree_names) {
            auto t = (TTree*) f->Get(tree_name.c_str());

            Double_t weight, mc_weight, mc_channelNumber, lumi_SF_fb;

            TBranch* b_weight = (TBranch*) t->GetListOfBranches()->FindObject(TString("weight"));
            if (b_weight) {
                WARNING("input file: " + f_name + " contains 'weight' branch already. Removing...");
                delete t;
                break;
            }
            b_weight = t->Branch("weight",&weight,"weight/D");

            t->SetBranchStatus("*"                , 0);
            t->SetBranchStatus("mc_channelNumber" , 1);
            t->SetBranchStatus("mc_weight"        , 1);
            t->SetBranchStatus("weight"           , 1);

            /* COMPUTE COMBINED MC WEIGHT */

            t->SetBranchAddress("mc_channelNumber" , &mc_channelNumber);
            t->SetBranchAddress("mc_weight"        , &mc_weight);
            t->SetBranchAddress("weight"           , &weight);

            const Long64_t total_entries = t->GetEntries();
            for (Long64_t entry = 0; entry < total_entries; entry++) {
                t->GetEntry(entry);

                if (entry == 0)
                {
                    if (mc_channelNumber <= 0) {
                        std::cout << "Attempted to pass data ntuple to post processing script. Exiting..." << std::endl;
                        exit(1);
                    }

                    lumi_SF_fb = weight_tool->get_lumi_sf(mc_channelNumber);
                }

                weight = mc_weight * lumi_SF_fb * luminosity;
                b_weight->Fill();
            }

            t->SetBranchStatus("*" , 1);
            t->Write("", TObject::kOverwrite);
            delete t;
        }

        if (doPdfReweight)
        {
            TTree* tree_nominal = (TTree*) f->Get("nominal");
            if (tree_nominal == nullptr)
                tree_nominal = (TTree*) f->Get("Nominal");

            if (tree_nominal == nullptr) {
                FATAL_ERROR("Couldn't load nominal branch in file for PDF reweighting");
            }

            tree_nominal->SetBranchStatus("*"      , 1);
            tree_nominal->SetBranchStatus("weight" , 0);

            TTree* tree_pdf_up = tree_nominal->CloneTree();
            TTree* tree_pdf_down = tree_nominal->CloneTree();

            std::string tmp_title = "ATLAS_PDF_1up";
            tree_pdf_up->SetTitle(tmp_title.c_str());
            tree_pdf_up->SetName(tmp_title.c_str());

            tmp_title = "ATLAS_PDF_1down";
            tree_pdf_down->SetTitle(tmp_title.c_str());
            tree_pdf_down->SetName(tmp_title.c_str());

            Double_t weight_nominal, weight_up, weight_down, x1, x2, pdgid1, pdgid2, Q;

            TBranch* b_weight_up = tree_pdf_up->Branch("weight",&weight_up,"weight/D");
            TBranch* b_weight_down = tree_pdf_down->Branch("weight",&weight_down,"weight/D");

            tree_nominal->SetBranchStatus("*"      , 0);
            tree_nominal->SetBranchStatus("weight" , 1);
            tree_nominal->SetBranchStatus("x1"     , 1);
            tree_nominal->SetBranchStatus("x2"     , 1);
            tree_nominal->SetBranchStatus("Q"      , 1);
            tree_nominal->SetBranchStatus("pdgid1" , 1);
            tree_nominal->SetBranchStatus("pdgid2" , 1);

            tree_nominal->SetBranchAddress("weight" , &weight_nominal);
            tree_nominal->SetBranchAddress("x1"     , &x1);
            tree_nominal->SetBranchAddress("x2"     , &x2);
            tree_nominal->SetBranchAddress("Q"      , &Q);
            tree_nominal->SetBranchAddress("pdgid1" , &pdgid1);
            tree_nominal->SetBranchAddress("pdgid2" , &pdgid2);

            tree_pdf_up->SetBranchAddress("weight"   , &weight_up);
            tree_pdf_down->SetBranchAddress("weight" , &weight_down);

            tree_pdf_up->SetBranchStatus("*"        , 0);
            tree_pdf_down->SetBranchStatus("*"      , 0);
            tree_pdf_up->SetBranchStatus("weight"   , 1);
            tree_pdf_down->SetBranchStatus("weight" , 1);

            for (Long64_t entry = 0; entry < tree_nominal->GetEntries(); entry++) {
                tree_nominal->GetEntry(entry);
                tree_pdf_up->GetEntry(entry);
                tree_pdf_down->GetEntry(entry);

                double nominal_pdf_weight = LHAPDF::weightxxQ(
                        pdgid1, pdgid2, x1, x2, Q,
                        nominal_pdf_set.second[0],
                        nominal_pdf_set.second[0]
                        );

                // compute error band for nominal pdf set.
                // NNPDF uses std. dev.
                // see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PdfRecommendations#Standard_deviation
                double sum_sqs = 0.0;
                for (size_t imem = 1; imem < nominal_pdf_set.second.size(); imem++) {
                    sum_sqs += pow(LHAPDF::weightxxQ(
                            pdgid1, pdgid2, x1, x2, Q,
                            nominal_pdf_set.second[0],
                            nominal_pdf_set.second[imem]
                            ) - nominal_pdf_weight, 2);
                }

                const double pdf_delta_sf = sqrt(1.0/ (nominal_pdf_set.second.size() - 1) * sum_sqs);
                double pdf_weight_up = nominal_pdf_weight + pdf_delta_sf;
                double pdf_weight_down = nominal_pdf_weight - pdf_delta_sf;

                // extend the 'envelope' in any places where the internal errors of our nominal
                // PDF set do *not* cover the discrepancy between the nominal PDF set and the other
                // additional PDF sets
                for (auto& pdf : additional_pdf_sets)
                {
                    double tmp_pdf_weight = LHAPDF::weightxxQ(
                            pdgid1, pdgid2, x1, x2, Q,
                            nominal_pdf_set.second[0],
                            pdf.second[0]
                            );

                    if (tmp_pdf_weight > pdf_weight_up) {
                        pdf_weight_up = tmp_pdf_weight;
                    } else if (tmp_pdf_weight < pdf_weight_down) {
                        pdf_weight_down = tmp_pdf_weight;
                    }
                }

                weight_up = weight_nominal * pdf_weight_up;
                weight_down = weight_nominal * pdf_weight_down;
                b_weight_up->Fill();
                b_weight_down->Fill();
            }

            tree_pdf_up->SetBranchStatus("*" , 1);
            tree_pdf_up->Write();
            delete tree_pdf_up;
            tree_pdf_down->SetBranchStatus("*" , 1);
            tree_pdf_down->Write();
            delete tree_pdf_down;
            delete tree_nominal;
        }

        f->Close();
        delete f;
    }

    return EXIT_SUCCESS;
}
