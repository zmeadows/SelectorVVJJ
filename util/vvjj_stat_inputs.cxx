#include <vector>
#include <iostream>

#include "TFile.h"
#include "TAttFill.h"
#include "TTree.h"
#include "TLeaf.h"

#include <iostream>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "SelectorVVJJ/util.h"

const std::vector<std::string> masses {
    "1000", "1100", "1200", "1300", "1400", "1500",
    "1600", "1700", "1800", "2000", "2200", "2400",
    "2600", "2800", "3000", "3500", "4000", "4500",
    "5000" };

const std::vector<std::string> trees = {
    "ATLAS_FATJET_D2R_up", "ATLAS_FATJET_JER_up", "ATLAS_FATJET_JMR_up", "ATLAS_JET_Rtrk_Baseline_D2_down",
    "ATLAS_JET_Rtrk_Baseline_D2_up", "ATLAS_JET_Rtrk_Baseline_Kin_down", "ATLAS_JET_Rtrk_Baseline_Kin_up",
    "ATLAS_JET_Rtrk_Modelling_D2_down", "ATLAS_JET_Rtrk_Modelling_D2_up", "ATLAS_JET_Rtrk_Modelling_Kin_down",
    "ATLAS_JET_Rtrk_Modelling_Kin_up", "ATLAS_JET_Rtrk_TotalStat_D2_down", "ATLAS_JET_Rtrk_TotalStat_D2_up",
    "ATLAS_JET_Rtrk_TotalStat_Kin_down", "ATLAS_JET_Rtrk_TotalStat_Kin_up", "ATLAS_JET_Rtrk_Tracking_D2_down",
    "ATLAS_JET_Rtrk_Tracking_D2_up", "ATLAS_JET_Rtrk_Tracking_Kin_down", "ATLAS_JET_Rtrk_Tracking_Kin_up",
    "nominal"};

const std::vector<std::string> signals = {"HVT_Agv1_VcWZ_qqqq", "HVT_Agv1_VzWW_qqqq", "RS_G_WW_qqqq_c10", "RS_G_ZZ_qqqq_c10"};

const std::string jet1W_sel = "first_jet_passedWMassCut50 && first_jet_passedWSubstructure50 && first_jet_ungrtrk500 < 30";
const std::string jet2W_sel = "second_jet_passedWMassCut50 && second_jet_passedWSubstructure50 && second_jet_ungrtrk500 < 30";
const std::string jet1Z_sel = "first_jet_passedZMassCut50 && first_jet_passedZSubstructure50 && first_jet_ungrtrk500 < 30";
const std::string jet2Z_sel = "second_jet_passedZMassCut50 && second_jet_passedZSubstructure50 && second_jet_ungrtrk500 < 30";

const std::string topo_sel =
    "n_jets >= 2 && "
    "lead_jet_m/1000. >= 50 && "
    "lead_jet_m/1000. <= 150 && "
    "sub_jet_m/1000. >= 50 && "
    "sub_jet_m/1000. <= 150 && "
    "sub_jet_m/1000. <= 150 && "
    "abs(first_jet_eta) < 2.0 && "
    "abs(second_jet_eta) < 2.0 && "
    "first_jet_pt/1000. > 200 && "
    "second_jet_pt/1000. > 200 && "
    "pass_grl && "
    "pass_evtclean && "
    "pass_vvjj_lepton_veto && "
    "met/1000. < 250 && "
    "mjj/1000 > 1100 && "
    "lead_jet_pt/1000. > 450 && "
    "sub_jet_pt/1000. > 450 && "
    "abs(dyjj)<1.2 && ptasym<0.15";

void
make_stat_input_tree(TTree* input_tree, const std::string& selection)
{
    std::cout << input_tree << std::endl;
    TTree* tmp_tree = input_tree->CopyTree(selection.c_str());
    //TTree* tmp_tree = input_tree->CopyTree("n_jets >= 2");

    tmp_tree->SetBranchStatus("*"      , 0);
    tmp_tree->SetBranchStatus("event"  , 1);
    tmp_tree->SetBranchStatus("run"    , 1);
    tmp_tree->SetBranchStatus("weight" , 1);
    tmp_tree->SetBranchStatus("mjj"    , 1);

    TTree* new_tree = tmp_tree->CloneTree();
    delete tmp_tree;

    new_tree->GetBranch("weight")->SetTitle("weight/F");
    new_tree->GetBranch("weight")->SetName("weight");
    new_tree->GetLeaf("weight")->SetTitle("weight/F");
    new_tree->GetLeaf("weight")->SetName("weight");
    new_tree->GetBranch("mjj")->SetTitle("x/F");
    new_tree->GetBranch("mjj")->SetName("x");
    new_tree->GetLeaf("mjj")->SetTitle("x/F");
    new_tree->GetLeaf("mjj")->SetName("x");
    new_tree->GetBranch("event")->SetTitle("eventNumber");
    new_tree->GetBranch("event")->SetName("eventNumber");
    new_tree->GetLeaf("event")->SetTitle("eventNumber");
    new_tree->GetLeaf("event")->SetName("eventNumber");
    new_tree->GetBranch("run")->SetTitle("runNumber");
    new_tree->GetBranch("run")->SetName("runNumber");
    new_tree->GetLeaf("run")->SetTitle("runNumber");
    new_tree->GetLeaf("run")->SetName("runNumber");
    new_tree->Print();
}

int
main(int argc, char* argv[])
{
    /* PROCESS COMMAND LINE ARGUMENTS */

    std::string signal_dir, data_dir, output_dir;

    try
    {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help"       , "produce help message")
            ("signal-dir" , po::value<std::string>(&signal_dir) , "directory containing signal ntuples")
            ("data-dir"   , po::value<std::string>(&data_dir) , "directory containing data ntuples")
            ("output-dir"   , po::value<std::string>(&output_dir) , "output directory")
            ;

        po::variables_map vm;
        po::store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return EXIT_SUCCESS;
        }

        po::notify(vm);
    }
    catch(std::exception& e) {
        FATAL_ERROR(e.what());
    }

    if (signal_dir.empty())
        FATAL_ERROR("no signal directory specified");

    if (data_dir.empty())
        FATAL_ERROR("no data directory specified");

    if (output_dir.empty())
        FATAL_ERROR("no output directory specified");

    if (signal_dir.back() != '/') signal_dir.append("/");
    if (data_dir.back() != '/') data_dir.append("/");
    if (output_dir.back() != '/') output_dir.append("/");


    for (const std::string& signal_topo : { "WW" , "WZ", "ZZ" })
        system( ("mkdir -p " + output_dir + "/" + signal_topo).c_str() );

    /* MAKE INPUTS FOR SIGNAL SAMPLES */

    for(auto const& signal : signals) {
        std::string selection;

        if (signal.find("WW") != std::string::npos) {
            selection = topo_sel + " && " + jet1W_sel + " && " + jet2W_sel;
        } else if (signal.find("ZZ") != std::string::npos) {
            selection = topo_sel + " && " + jet1Z_sel + " && " + jet2Z_sel;
        } else if (signal.find("WZ") != std::string::npos) {
            selection = topo_sel + " && " + jet1Z_sel + " && " + jet2W_sel;
        } else {
            FATAL_ERROR("Signal " + signal + " not WW, ZZ, or WZ?");
        }

        for (auto const& mass : masses) {
            TFile* output_file;

            if (signal.find("WW") != std::string::npos) {
                output_file = TFile::Open( (output_dir + "WW/" + signal + "_m" + mass + ".root").c_str(), "RECREATE" );
            } else if (signal.find("ZZ") != std::string::npos) {
                output_file = TFile::Open( (output_dir + "ZZ/" + signal + "_m" + mass + ".root").c_str(), "RECREATE" );
            } else if (signal.find("WZ") != std::string::npos) {
                output_file = TFile::Open( (output_dir + "WZ/" + signal + "_m" + mass + ".root").c_str(), "RECREATE" );
            } else {
                FATAL_ERROR("Signal " + signal + " not WW, ZZ, or WZ?");
            }

            for (auto const& tree : trees) {
                const std::vector<std::string> input_file_list =
                    tree == "nominal"
                    ? glob(signal_dir + signal + "*" + mass + "*" + "Nominal" + "/*/tree_0.root")
                    : glob(signal_dir + signal + "*" + mass + "*" + tree + "/*/tree_0.root");

                if (input_file_list.size() == 0) {
                    WARNING(
                            "Could not find file for\n"
                            "\t\tsample: " + signal + "\n"
                            "\t\tmass: " + mass + "\n"
                            "\t\ttree: " + tree
                           );
                    continue;
                } else if (input_file_list.size() > 1) {
                    WARNING(
                            "Found more than one file for\n"
                            "\t\tsample: " + signal + "\n"
                            "\t\tmass: " + mass + "\n"
                            "\t\ttree: " + tree
                           );
                    continue;
                }

                std::cout << "FILE: " << input_file_list[0]  << std::endl;
                std::cout << "TREE: " << tree << std::endl;

                TFile* input_file = TFile::Open(input_file_list[0].c_str(), "READ");
                if (!input_file) break;
                TTree* old_tree = (TTree*) input_file->Get(tree.c_str());

                output_file->cd();
                make_stat_input_tree(old_tree, selection);

                input_file->Close();
                delete input_file;
            }

            output_file->Write();
            output_file->Close();
            delete output_file;
        }
    }

    /* MAKE INPUTS FOR DATA */

    return EXIT_SUCCESS;
}
