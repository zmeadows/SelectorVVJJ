#include "SelectorVVJJ/Selection.h"
#include "SelectorVVJJ/TH1Tagged.h"
#include "SelectorVVJJ/util.h"
#include "SelectorVVJJ/VVJJSelector.h"
#include "SelectorVVJJ/WeightTool.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class VVJJSelector+;
#pragma link C++ class TH1Tagged+;
#pragma link C++ class WeightTool+;
#pragma link C++ class Globber+;

#endif
