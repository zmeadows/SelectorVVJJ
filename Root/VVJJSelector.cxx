#define VVJJSelector_cxx

#include "SelectorVVJJ/VVJJSelector.h"

#include <TH2.h>
#include <TStyle.h>
#include <TLorentzVector.h>

#include "SelectorVVJJ/Selection.h"
#include "SelectorVVJJ/util.h"

// {{{ SELECTIONS

bool
passVVJJBaselineSelection(const VVJJSelector* const sel, Selection* sss)
{
    if (sel->vars.at("n_jets") < 2) return false;
    sss->bump("JetContainer Size", sel->my_weight);

    if (sel->vars.at("lead_jet_m") < 50*GeV || sel->vars.at("lead_jet_m") > 150*GeV
            || sel->vars.at("sub_jet_m") < 50*GeV || sel->vars.at("sub_jet_m") > 150*GeV) return false;
    sss->bump("Lead/Sub Jet Mass in [50,150] GeV", sel->my_weight);

    if (fabs(sel->vars.at("first_jet_eta")) >= 2.0 || fabs(sel->vars.at("second_jet_eta")) >= 2.0)
        return false;
    sss->bump("Jet #eta", sel->my_weight);

    if (sel->vars.at("first_jet_pt") <= 200*GeV || sel->vars.at("second_jet_pt") <= 200*GeV)
        return false;
    sss->bump("Jet p_{T}", sel->my_weight);

    if (!sel->vars.at("pass_grl"))
        return false;
    sss->bump("GRL", sel->my_weight);

    if (!sel->vars.at("pass_HLT_j420_a10_lcw_L1J100") && !sel->operating_on_mc)
        return false;
    sss->bump("TRIGGER", sel->my_weight);

    if (!sel->vars.at("pass_evtclean"))
        return false;
    sss->bump("Event Cleaning", sel->my_weight);

    if (!sel->vars.at("pass_jetclean"))
        return false;
    sss->bump("Jet Cleaning", sel->my_weight);

    if (!sel->vars.at("pass_vvjj_lepton_veto"))
        return false;
    sss->bump("Lepton Veto", sel->my_weight);

    if (sel->vars.at("met") / 1000. >= 250*GeV)
        return false;
    sss->bump("MET", sel->my_weight);

    if (sel->vars.at("mjj") <= 1.1 * TeV) return false;
    sss->bump("m_{JJ}", sel->my_weight);

    if (sel->vars.at("lead_jet_pt") <= 450. * GeV && sel->vars.at("sub_jet_pt") <= 450. * GeV)
        return false;
    sss->bump("Leading Jet p_{T}", sel->my_weight);

    return true;
}


bool
passVVJJTopoSelection(const VVJJSelector* const sel, Selection* sss)
{
    if (fabs(sel->vars.at("dyjj")) >= 1.2) return false;
    sss->bump("#Delta Y < 1.2", sel->my_weight);

    if (fabs(sel->vars.at("ptasym")) >= 0.15) return false;
    sss->bump("p_{T} Asym < 0.15", sel->my_weight);

    return true;
}

/*

bool
passVBFqqqqPreSelection(const VVJJSelector* const sel, Selection* sss, const bool do_bumps)
{
    if (sel->vars.at("jet_pt")->size() != 2)
        return false;
    if (do_bumps) sss->bump("2 Large Jets", sel->my_weight);

    if (sel->vars.at("akt4_jet_pt")->size() < 2)
        return false;
    if (do_bumps) sss->bump("Num SmallRJets", sel->my_weight);

    const double MIN_ETA_GAP = 3;
    const double SMALL_LARGE_OVERLAP_DR_MIN = 1.;
    const double TAG_DIJET_MASS_MIN = 500 * GeV;
    const double MV2c10_CUT = 0.8244273;

    std::map<int,TLorentzVector> small_jets;
    std::vector<TLorentzVector> large_jets;

    int num_small_jets = sel->vars.at("akt4_jet_pt")->size();
    for (auto i = 0; i < num_small_jets; i++) {
        TLorentzVector v_tmp;
        v_tmp.SetPtEtaPhiM(
                sel->vars.at("akt4_jet_pt")->at(i),
                sel->vars.at("akt4_jet_eta")->at(i),
                sel->vars.at("akt4_jet_phi")->at(i),
                sel->vars.at("akt4_jet_mass")->at(i)
                );
        small_jets.emplace(i,v_tmp);
    }

    int num_large_jets = sel->vars.at("jet_pt")->size();
    for (auto i = 0; i < num_large_jets; i++) {
        TLorentzVector v_tmp;
        v_tmp.SetPtEtaPhiM(
                sel->vars.at("jet_pt")->at(i),
                sel->vars.at("jet_eta")->at(i),
                sel->vars.at("jet_phi")->at(i),
                sel->vars.at("jet_mass")->at(i)
                );
        large_jets.push_back(v_tmp);
    }

    auto inside_large_jet = [&](const TLorentzVector& small_jet) -> bool {
        for (unsigned i = 0; i < large_jets.size(); i++) {
            if (fabs(small_jet.DeltaR(large_jets[i])) < SMALL_LARGE_OVERLAP_DR_MIN)
                return true;
        }
        return false;
    };

    std::vector<int> tag_candidates;

    for (auto const& x : small_jets) {
        int tag_cand_index = x.first;
        TLorentzVector tag_cand_jet = x.second;

        // small-R jets that overlap with one of the two large-R jets
        // are expected and ignored
        if (!inside_large_jet(tag_cand_jet)) {
            tag_candidates.push_back(tag_cand_index);
            if (do_bumps) sss->bump("Cent. Iso. Small Jets", sel->my_weight);
        }

        // const bool in_central_region = fabs(tag_cand_jet.Eta()) < MIN_ABS_ETA;

        // if (in_central_region) {
        //     return false;
        // } else {
        //     tag_candidates.push_back(tag_cand_index);
        //     if (do_bumps) sss->bump("Cent. Iso. Small Jets");
        // }
    }

    if (tag_candidates.size() < 2)
        return false;
    if (do_bumps) sss->bump("Two Tag Cands", sel->my_weight);

    // note: tag_candidates is still pT ordered
    TLorentzVector tagjet0 = small_jets[tag_candidates[0]];
    TLorentzVector tagjet1 = small_jets[tag_candidates[1]];

    if (fabs (tagjet0.Eta() - tagjet1.Eta()) < MIN_ETA_GAP)
        return false;
    if (do_bumps) sss->bump("|#Delta#eta|_{tag} gap", sel->my_weight);

    if ( (tagjet0 + tagjet1).M() < TAG_DIJET_MASS_MIN)
        return false;
    if (do_bumps) sss->bump("m_{jj}^{tag}", sel->my_weight);

    if (sel->akt4_jet_mv2c10->at(tag_candidates[0]) > MV2c10_CUT)
        return false;
    if (do_bumps) sss->bump("Lead tagjet b-veto", sel->my_weight);

    if (sel->akt4_jet_mv2c10->at(tag_candidates[1]) > MV2c10_CUT)
        return false;
    if (do_bumps) sss->bump("Sublead tagjet b-veto", sel->my_weight);

    sel->set_tagjet0(tagjet0);
    sel->set_tagjet0(tagjet1);

    return true;
}

bool
passVVJJPreSelectionVBFVeto(const VVJJSelector* const sel, Selection* sss)
{
    if (!passVVJJPreSelection(sel, sss))
        return false;
    sss->bump("VVJJ PreSel", sel->my_weight);

    if (passVBFqqqqPreSelection(sel, sss, false))
        return false;
    sss->bump("VBF Veto", sel->my_weight);

    return true;
}

*/

bool
passWWtag(const VVJJSelector* const sel, Selection* sss)
{
    if (!sel->vars.at("lead_jet_passedWMassCut50")) return false;
    sss->bump("Lead Jet Mass Tag", sel->my_weight);

    if (!sel->vars.at("sub_jet_passedWMassCut50")) return false;
    sss->bump("Second Jet Mass Tag", sel->my_weight);

    if (!sel->vars.at("lead_jet_passedWSubstructure50")) return false;
    sss->bump("Lead Jet D2 Tag", sel->my_weight);

    if (!sel->vars.at("sub_jet_passedWSubstructure50")) return false;
    sss->bump("Second Jet D2 Tag", sel->my_weight);

    if (sel->vars.at("lead_jet_ungrtrk500") >= 30) return false;
    sss->bump("Lead Jet n_{trk}", sel->my_weight);

    if (sel->vars.at("sub_jet_ungrtrk500") >= 30) return false;
    sss->bump("second Jet n_{trk}", sel->my_weight);

    return true;
}

bool
passWZtag(const VVJJSelector* const sel, Selection* sss)
{
    if (!sel->vars.at("first_jet_passedZMassCut50")) return false;
    sss->bump("First Jet Mass Tag", sel->my_weight);

    if (!sel->vars.at("second_jet_passedWMassCut50")) return false;
    sss->bump("Second Jet Mass Tag", sel->my_weight);

    if (!sel->vars.at("first_jet_passedZSubstructure50")) return false;
    sss->bump("First Jet D2 Tag", sel->my_weight);

    if (!sel->vars.at("second_jet_passedWSubstructure50")) return false;
    sss->bump("Second Jet D2 Tag", sel->my_weight);

    if (sel->vars.at("first_jet_ungrtrk500") >= 30) return false;
    sss->bump("First Jet n_{trk}", sel->my_weight);

    if (sel->vars.at("second_jet_ungrtrk500") >= 30) return false;
    sss->bump("second Jet n_{trk}", sel->my_weight);

    return true;
}

bool
passZZtag(const VVJJSelector* const sel, Selection* sss)
{
    if (!sel->vars.at("lead_jet_passedZMassCut50")) return false;
    sss->bump("Lead Jet Mass Tag", sel->my_weight);

    if (!sel->vars.at("sub_jet_passedZMassCut50")) return false;
    sss->bump("Second Jet Mass Tag", sel->my_weight);

    if (!sel->vars.at("lead_jet_passedZSubstructure50")) return false;
    sss->bump("Lead Jet D2 Tag", sel->my_weight);

    if (!sel->vars.at("sub_jet_passedZSubstructure50")) return false;
    sss->bump("Second Jet D2 Tag", sel->my_weight);

    if (sel->vars.at("lead_jet_ungrtrk500") >= 30) return false;
    sss->bump("Lead Jet n_{trk}", sel->my_weight);

    if (sel->vars.at("sub_jet_ungrtrk500") >= 30) return false;
    sss->bump("second Jet n_{trk}", sel->my_weight);

    return true;
}
// }}}

void VVJJSelector::Begin(TTree * /*tree*/)
{
   // The Begin() function is called at the start of the query.
   // When running with PROOF Begin() is only called on the client.
   // The tree argument is deprecated (on PROOF 0 is passed).

}

void VVJJSelector::SlaveBegin(TTree * /*tree*/)
{
    // The SlaveBegin() function is called after the Begin() function.
    // When running with PROOF SlaveBegin() is called on each slave server.
    // The tree argument is deprecated (on PROOF 0 is passed).

    TString option = GetOption();

    // Info("woooo", option.Data());

    if (option == "VVJJPreSelection")
        selection = new VVJJPreSelection();
    else if (option == "VVJJBaselineSelection")
        selection = new VVJJBaselineSelection();
    else if (option == "VVJJSelectionWW")
        selection = new VVJJSelectionWW();
    else if (option == "VVJJSelectionWZ")
        selection = new VVJJSelectionWZ();
    else if (option == "VVJJSelectionZZ")
        selection = new VVJJSelectionZZ();
    else if (option == "NoSelection")
        selection = nullptr;
    // else if (option == "VVJJPreSelectionVBFVetoWW")
    //     selection = new VVJJPreSelectionVBFVetoWW();
    // else if (option == "VVJJPreSelectionVBFVetoWZ")
    //     selection = new VVJJPreSelectionVBFVetoWZ();
    // else if (option == "VVJJPreSelectionVBFVetoZZ")
    //     selection = new VVJJPreSelectionVBFVetoZZ();
    else FATAL_ERROR("Selection: " + std::string(option.Data()) + " not recognized!");

    std::cout << "SAMPLE: " << sample_type << std::endl;
    std::cout << "TREE: " << tree_name << std::endl;
}

Bool_t VVJJSelector::Process(Long64_t entry)
{
    // The Process() function is called for each entry in the tree (or possibly
    // keyed object in the case of PROOF) to be processed. The entry argument
    // specifies which entry in the currently loaded tree is to be processed.
    // It can be passed to either VVJJSelector::GetEntry() or TBranch::GetEntry()
    // to read either all or the required parts of the data. When processing
    // keyed objects with PROOF, the object is already loaded and is available
    // via the fObject pointer.
    //
    // This function should contain the "body" of the analysis. It can contain
    // simple or elaborate selection criteria, run algorithms on the data
    // of the event and typically fill histograms.
    //
    // The processing can be stopped by calling Abort().
    //
    // Use fStatus to set the return value of TTree::Process().
    //
    // The return value is currently not used.

    this->GetEntry(entry);

    if (entry == 0)
        std::cout << "processing " << this->fChain->GetEntries() << " entries." << std::endl;
    num_entries_processed++;

    double percent_done = 100 * (float) num_entries_processed / (float) this->fChain->GetEntries();
    if (percent_done >= next_print_percent) {
        std::cout << next_print_percent << "%..." << std::flush;
        if (percent_done >= 100.0) {
            std::cout << "DONE.\n" << std::endl;
        } else {
            next_print_percent += 10.0;
        }
    }

    // no need to ever process data that doesn't pass 'sanity cuts'
    if (!operating_on_mc) {
        if (!vars.at("pass_grl")
                || !vars.at("pass_jetclean")
                || !vars.at("pass_evtclean")
                || !vars.at("pass_HLT_j420_a10_lcw_L1J100"))
            return kFALSE;
    }

    if (selection && !selection->check(this))
        return kFALSE;

    // tagjet0.SetPtEtaPhiM(0,0,0,0);
    // tagjet1.SetPtEtaPhiM(0,0,0,0);

    // at this point, no pileup weight applied
    my_weight = 1.0;
    if (operating_on_mc) my_weight = vars.at("weight");

    bool pass_baseline = true;

    if (!selection)
    { // baseline is part of every Selection except 'NoSelection', so we duplicate it for that case
        if (vars.at("n_jets") < 2) pass_baseline = false;

        if (vars.at("lead_jet_m") < 50*GeV || vars.at("lead_jet_m") > 150*GeV
                || vars.at("sub_jet_m") < 50*GeV || vars.at("sub_jet_m") > 150*GeV)
            pass_baseline = false;

        if (fabs(vars.at("first_jet_eta")) >= 2.0 || fabs(vars.at("second_jet_eta")) >= 2.0)
            pass_baseline = false;

        if (vars.at("first_jet_pt") <= 200*GeV || vars.at("second_jet_pt") <= 200*GeV)
            pass_baseline = false;

        if (!vars.at("pass_vvjj_lepton_veto"))
            pass_baseline = false;

        if (vars.at("met") / 1000. >= 250*GeV)
            pass_baseline = false;

        if (vars.at("mjj") <= 1.1 * TeV)
            pass_baseline = false;

        if (vars.at("lead_jet_pt") <= 450. * GeV && vars.at("sub_jet_pt") <= 450. * GeV)
            pass_baseline = false;
    }

    selection_regions["noSelection"]  = true;
    selection_regions["noTopo"]       = pass_baseline;
    selection_regions["TopoNodyjj"]   = pass_baseline && vars.at("ptasym") < 0.15;
    selection_regions["TopoNoptasym"] = pass_baseline && vars.at("dyjj") < 1.2;

    const bool pass_topo      = pass_baseline && vars.at("dyjj") < 1.2 && vars.at("ptasym") < 0.15;
    selection_regions["Topo"] = pass_topo;

    const bool isJet1W50 = vars.at("first_jet_passedWMassCut50")  && vars.at("first_jet_passedWSubstructure50");
    const bool isJet2W50 = vars.at("second_jet_passedWMassCut50") && vars.at("second_jet_passedWSubstructure50");
    const bool isJet1Z50 = vars.at("first_jet_passedZMassCut50")  && vars.at("first_jet_passedZSubstructure50");
    const bool isJet2Z50 = vars.at("second_jet_passedZMassCut50") && vars.at("second_jet_passedZSubstructure50");

    const bool pass_ntrk_both_jets = vars.at("first_jet_ungrtrk500") < 30   && vars.at("second_jet_ungrtrk500") < 30;

    selection_regions["WWNoNtrk"] = pass_topo && isJet1W50 && isJet2W50;
    selection_regions["ZZNoNtrk"] = pass_topo && isJet1Z50 && isJet2Z50;
    selection_regions["WZNoNtrk"] = pass_topo && isJet1Z50 && isJet2W50;


    selection_regions["SRWW"] = pass_topo && isJet1W50 && isJet2W50 && pass_ntrk_both_jets;
    selection_regions["SRZZ"] = pass_topo && isJet1Z50 && isJet2Z50 && pass_ntrk_both_jets;
    selection_regions["SRWZ"] = pass_topo && isJet1Z50 && isJet2W50 && pass_ntrk_both_jets;

    selection_regions["WWMassOnly"] = pass_topo && vars.at("first_jet_passedWMassCut50") && vars.at("second_jet_passedWMassCut50");
    selection_regions["WZMassOnly"] = pass_topo && vars.at("first_jet_passedZMassCut50") && vars.at("second_jet_passedWMassCut50");
    selection_regions["ZZMassOnly"] = pass_topo && vars.at("first_jet_passedZMassCut50") && vars.at("second_jet_passedZMassCut50");

    selection_regions["WWD2Only"] = pass_topo && vars.at("first_jet_passedWSubstructure50") && vars.at("second_jet_passedWSubstructure50");
    selection_regions["ZZD2Only"] = pass_topo && vars.at("first_jet_passedZSubstructure50") && vars.at("second_jet_passedZSubstructure50");
    selection_regions["WZD2Only"] = pass_topo && vars.at("first_jet_passedZSubstructure50") && vars.at("second_jet_passedWSubstructure50");

    selection_regions["NtrkOnly"] = pass_topo && pass_ntrk_both_jets;

    selection_regions["VRLowMass"] =
                pass_topo
                && !vars.at("first_jet_passedWMassCut50")
                && vars.at("first_jet_m") < 80*GeV
                && vars.at("first_jet_passedWSubstructure80")
                && !vars.at("second_jet_passedWMassCut50")
                && vars.at("second_jet_m") < 80*GeV
                && vars.at("second_jet_m") > 50*GeV
                && vars.at("second_jet_passedWSubstructure50");

    selection_regions["VRHighMass"] =
                pass_topo
                && !vars.at("first_jet_passedZMassCut50")
                && vars.at("first_jet_m") > 90*GeV
                && vars.at("first_jet_m") < 140*GeV
                && vars.at("first_jet_passedZSubstructure50")
                && !vars.at("second_jet_passedZMassCut50")
                && vars.at("second_jet_m") > 90*GeV
                && vars.at("second_jet_m") < 140*GeV
                && vars.at("second_jet_passedZSubstructure50");

    selection_regions["VRMixedMass"] =
                pass_topo
                && !vars.at("first_jet_passedZMassCut50")
                && vars.at("first_jet_m") > 90*GeV
                && vars.at("first_jet_m") < 140*GeV
                && vars.at("first_jet_passedZSubstructure50")
                && !vars.at("second_jet_passedWMassCut50")
                && vars.at("second_jet_m") < 80*GeV
                && vars.at("second_jet_m") > 50*GeV
                && vars.at("second_jet_passedWSubstructure50");

    selection_regions["WWNoMass"] = pass_topo && vars.at("first_jet_passedWSubstructure50") && vars.at("second_jet_passedWSubstructure50") && pass_ntrk_both_jets;
    selection_regions["ZZNoMass"] = pass_topo && vars.at("first_jet_passedZMassCut50") && vars.at("second_jet_passedZMassCut50") && pass_ntrk_both_jets;
    selection_regions["WZNoMass"] = pass_topo && vars.at("first_jet_passedZSubstructure50") && vars.at("second_jet_passedWSubstructure50") && pass_ntrk_both_jets;

    selection_regions["WWNoD2"] = pass_topo && vars.at("first_jet_passedWMassCut50") && vars.at("second_jet_passedWMassCut50") && pass_ntrk_both_jets;
    selection_regions["ZZNoD2"] = pass_topo && vars.at("first_jet_passedZMassCut50") && vars.at("second_jet_passedZMassCut50") && pass_ntrk_both_jets;
    selection_regions["WZNoD2"] = pass_topo && vars.at("first_jet_passedZMassCut50") && vars.at("second_jet_passedWMassCut50") && pass_ntrk_both_jets;

   const bool isJet1W80 = vars.at("first_jet_passedWMassCut80")
               && vars.at("first_jet_passedWSubstructure80");

   const bool isJet2W80 = vars.at("second_jet_passedWMassCut80")
               && vars.at("second_jet_passedWSubstructure80");

   const bool isJet1Z80 = vars.at("first_jet_passedZMassCut80")
               && vars.at("first_jet_passedZSubstructure80");

   const bool isJet2Z80 = vars.at("second_jet_passedZMassCut80")
               && vars.at("second_jet_passedZSubstructure80");

   selection_regions["VRLooseWW"] = pass_topo && isJet1W80 && isJet2W80 && pass_ntrk_both_jets;
   selection_regions["VRLooseZZ"] = pass_topo && isJet1Z80 && isJet2Z80 && pass_ntrk_both_jets;
   selection_regions["VRLooseWZ"] = pass_topo && isJet1Z80 && isJet2W80 && pass_ntrk_both_jets;

   // PRE/POST ICHEP SELECTIONS
   for (const auto& sel : selection_regions) {
       prepost_ICHEP_selection_regions[sel.first + std::string("PreICHEP")] =
           sel.second && vars.at("pass_grlA");
       prepost_ICHEP_selection_regions[sel.first + std::string("PostICHEP")] =
           sel.second && vars.at("pass_grlB");
   }

    // MJJ WINDOW SELECTIONS
    //const std::vector<unsigned> mjj_benchmarks = { 1500, 2000, 2400, 3000, 4000, 5000 };
    const std::vector<unsigned> mjj_benchmarks = {};

    for (auto const& region_map : {selection_regions, prepost_ICHEP_selection_regions})
    {
        for (const auto& sel : region_map)
        {
            for (auto const& mass_point : mjj_benchmarks) {
                if (!contains("SR",sel.first) && !contains("VR",sel.first)) {
                    const std::string mass_sel_str = sel.first + std::string("Mjj10percWind") + std::to_string(mass_point);
                    mjj_window_selection_regions[mass_sel_str] = sel.second && fabs(vars.at("mjj") - mass_point * GeV) < 0.1 * mass_point * GeV;
                }
            }
        }
    }

    /*******************/
    /* FILL HISTOGRAMS */
    /*******************/

    fill_hist_regions(selection_regions);
    // fill_hist_regions(mjj_window_selection_regions);
    // fill_hist_regions(prepost_ICHEP_selection_regions);

    return kTRUE;
}

void
VVJJSelector::fill_hist_regions(const std::unordered_map<std::string, bool>& regions)
{
    double weight;
    for (auto const& region : regions)
    {
        const bool tag_passed = region.second;
        if (!tag_passed && num_entries_processed > 0) continue;
        const std::string tag_name = region.first;

        if (operating_on_mc) {
            if (tag_name.find("PreICHEP") != std::string::npos) {
                weight = my_weight * vars.at("pileup_weightA");
            } else if (tag_name.find("PostICHEP") != std::string::npos) {
                weight = my_weight * vars.at("pileup_weightB");
            } else {
                weight = my_weight * vars.at("pileup_weight");
            }
        } else {
            weight = my_weight;
        }

        hists.at("h_lead_jet_ntrk")->fill_tagged(tag_name   , vars.at("lead_jet_ungrtrk500")   , weight , tag_passed);
        hists.at("h_sub_jet_ntrk")->fill_tagged(tag_name    , vars.at("sub_jet_ungrtrk500")    , weight , tag_passed);
        hists.at("h_first_jet_ntrk")->fill_tagged(tag_name  , vars.at("first_jet_ungrtrk500")  , weight , tag_passed);
        hists.at("h_second_jet_ntrk")->fill_tagged(tag_name , vars.at("second_jet_ungrtrk500") , weight , tag_passed);

        if (contains("ICHEP", tag_name)) continue;

        hists.at("h_lead_jet_pt")->fill_tagged(tag_name    , vars.at("lead_jet_pt") / 1000.   , weight , tag_passed);
        hists.at("h_lead_jet_eta")->fill_tagged(tag_name   , vars.at("lead_jet_eta")          , weight , tag_passed);
        hists.at("h_lead_jet_phi")->fill_tagged(tag_name   , vars.at("lead_jet_phi")          , weight , tag_passed);
        hists.at("h_lead_jet_m")->fill_tagged(tag_name     , vars.at("lead_jet_m") / 1000.    , weight , tag_passed);
        hists.at("h_lead_jet_D2")->fill_tagged(tag_name    , vars.at("lead_jet_D2")           , weight , tag_passed);
        hists.at("h_sub_jet_pt")->fill_tagged(tag_name     , vars.at("sub_jet_pt") / 1000.    , weight , tag_passed);
        hists.at("h_sub_jet_eta")->fill_tagged(tag_name    , vars.at("sub_jet_eta")           , weight , tag_passed);
        hists.at("h_sub_jet_phi")->fill_tagged(tag_name    , vars.at("sub_jet_phi")           , weight , tag_passed);
        hists.at("h_sub_jet_m")->fill_tagged(tag_name      , vars.at("sub_jet_m") / 1000.     , weight , tag_passed);
        hists.at("h_sub_jet_D2")->fill_tagged(tag_name     , vars.at("sub_jet_D2")            , weight , tag_passed);
        hists.at("h_first_jet_pt")->fill_tagged(tag_name   , vars.at("first_jet_pt") / 1000.  , weight , tag_passed);
        hists.at("h_first_jet_eta")->fill_tagged(tag_name  , vars.at("first_jet_eta")         , weight , tag_passed);
        hists.at("h_first_jet_phi")->fill_tagged(tag_name  , vars.at("first_jet_phi")         , weight , tag_passed);
        hists.at("h_first_jet_m")->fill_tagged(tag_name    , vars.at("first_jet_m") / 1000.   , weight , tag_passed);
        hists.at("h_first_jet_D2")->fill_tagged(tag_name   , vars.at("first_jet_D2")          , weight , tag_passed);
        hists.at("h_second_jet_pt")->fill_tagged(tag_name  , vars.at("second_jet_pt") / 1000. , weight , tag_passed);
        hists.at("h_second_jet_eta")->fill_tagged(tag_name , vars.at("second_jet_eta")        , weight , tag_passed);
        hists.at("h_second_jet_phi")->fill_tagged(tag_name , vars.at("second_jet_phi")        , weight , tag_passed);
        hists.at("h_second_jet_m")->fill_tagged(tag_name   , vars.at("second_jet_m") / 1000.  , weight , tag_passed);
        hists.at("h_second_jet_D2")->fill_tagged(tag_name  , vars.at("second_jet_D2")         , weight , tag_passed);
        hists.at("h_mjj")->fill_tagged(tag_name            , vars.at("mjj") / 1000.           , weight , tag_passed);
        hists.at("h_ptasym")->fill_tagged(tag_name         , vars.at("ptasym")                , weight , tag_passed);
        hists.at("h_dyjj")->fill_tagged(tag_name           , vars.at("dyjj")                  , weight , tag_passed);
        hists.at("h_meanY")->fill_tagged(tag_name          , vars.at("meanY")                 , weight , tag_passed);

        if (contains("Mjj10perc", tag_name)) {
            double xmin = hists.at("h_ntrkEffNum")->x_min;
            double xmax = hists.at("h_ntrkEffNum")->x_max;
            double delta = (xmax - xmin) / hists.at("h_ntrkEffNum")->num_bins;

            for (auto i = xmax - delta/2; i >= xmin; i -= delta)
            {
                if (vars.at("lead_jet_ungrtrk500") < i && vars.at("sub_jet_ungrtrk500") < i)
                    hists.at("h_ntrkEffNum")->fill_tagged(tag_name, i, weight, tag_passed);

                // for (const std::string& j_str : {"lead", "sub", "first", "second"}) {
                //     if (vars.at(j_str + "_jet_ungrtrk500") < i)
                //         hists.at("h_ntrkEffNum")->fill_tagged(j_str + "_" + tag_name,
                //                 i, weight, tag_passed);
                // }
            }

            xmin = hists.at("h_ptasymEffNum")->x_min;
            xmax = hists.at("h_ptasymEffNum")->x_max;
            delta = (xmax - xmin) / hists.at("h_ptasymEffNum")->num_bins;

            for (auto i = xmax - delta/2; i >= xmin; i -= delta)
            {
                if (vars.at("ptasym") < i)
                    hists.at("h_ptasymEffNum")->fill_tagged(tag_name, i, weight, tag_passed);
            }

            xmin = hists.at("h_dyjjEffNum")->x_min;
            xmax = hists.at("h_dyjjEffNum")->x_max;
            delta = (xmax - xmin) / hists.at("h_dyjjEffNum")->num_bins;

            for (auto i = xmax - delta/2; i >= xmin; i -= delta)
            {
                if (vars.at("dyjj") < i)
                    hists.at("h_dyjjEffNum")->fill_tagged(tag_name, i, weight, tag_passed);
            }
        }
    }
}

void VVJJSelector::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.
}

void VVJJSelector::Terminate()
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.

   if (selection) selection->print_cutflow();

   TDirectory* top_dir = output_file->GetDirectory(sample_type.c_str());
   if (!top_dir)
       top_dir = output_file->mkdir(sample_type.c_str());
   top_dir->cd();

   TDirectory* tree_dir = top_dir->mkdir(tree_name.c_str());
   tree_dir->cd();

   for (auto& h : hists) {
       h.second->write_all();
       delete h.second;
   }

   // double vbf_veto_percent = 100.0 *
   //     (double) selection->get_cut_count("VBF Veto")
   //     / (double) selection->get_cut_count("VVJJ PreSel");
   // std::cout << "(VVJJPreSel + VBF Veto) / VVJJPreSel: " <<
   //     vbf_veto_percent << " %" << std::endl;
}
