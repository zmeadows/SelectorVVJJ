#include "SelectorVVJJ/util.h"

#include <TH1F.h>

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <algorithm>

void
FATAL_ERROR(const std::string& msg)
{
    std::cout << "ERROR: " << msg << std::endl;
    exit(EXIT_FAILURE);
}

void
WARNING(const std::string& msg)
{
    std::cout << "WARNING: " << msg << std::endl;
}

void
print_strings(const std::vector<std::string>& strings)
{
    for (auto const& s : strings)
        std::cout << s << " ";

    std::cout << std::endl;
}

TH1F* makeTH1F(
        const std::string& name,
        const int num_bins,
        const float x_min,
        const float x_max,
        const std::string& x_axis_title
        )
{
    auto h_tmp = new TH1F(name.c_str(), name.c_str(), num_bins, x_min, x_max);
    h_tmp->Sumw2();
    h_tmp->SetDirectory(0);

    h_tmp->GetXaxis()->SetTitle(x_axis_title.c_str());
    h_tmp->GetYaxis()->SetTitle("Events");
    return (TH1F*) h_tmp->Clone();
}

std::vector<std::string> get_tree_names(const TFile* input_file)
{
        TIter next_key(input_file->GetListOfKeys());

        std::set<std::string> tree_names;

        while (auto tmp_key = (TKey*) next_key()) {
                std::string tmp_key_title(tmp_key->GetTitle());
                std::string tmp_key_name(tmp_key->GetName());

                bool is_tree = strcmp(tmp_key->GetClassName(),"TTree") == 0;

                bool is_relevant_tree = tmp_key_name == "nominal" ||
                        has_suffix(tmp_key_name, "1up") ||
                        has_suffix(tmp_key_name, "1down");

                if (is_tree && is_relevant_tree)
                        tree_names.insert(tmp_key_name);
        }

        return std::vector<std::string>(tree_names.begin(), tree_names.end());
}

bool has_suffix(const std::string& str, const std::string& suffix)
{
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

static std::pair<std::string, std::string> SplitPath(const std::string &path) {
    std::string::size_type last_sep = path.find_last_of("/");
    if (last_sep != std::string::npos) {
        return std::make_pair(std::string(path.begin(), path.begin() + last_sep),
                std::string(path.begin() + last_sep + 1, path.end()));
    }
    return std::make_pair(".", path);
}


