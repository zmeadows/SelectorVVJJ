#include "SelectorVVJJ/WeightTool.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

WeightTool::WeightTool(void) { }

bool
WeightTool::read_weights_file(const std::string& filepath)
{
	std::ifstream samples_file(filepath, std::ifstream::in);

	if (!samples_file.is_open()) {
		std::cout << "failed to open sample xsection file: " << filepath << std::endl;
		return false;
	}

	std::string line, sample_name;
	float nevents, xsection, filtereff;

	while (std::getline(samples_file, line))
	{
		if (line[0] != '#') {
			std::istringstream iss(line);
			if (!(iss >> sample_name >> nevents >> xsection >> filtereff)) {
				std::cout << "failed to parse line: " << line << std::endl;
				m_nevents_map.clear();
				m_xsection_map.clear();
				m_filtereff_map.clear();
				return false;
			} else {
				m_nevents_map[sample_name]   = nevents;
				m_xsection_map[sample_name]  = xsection;
				m_filtereff_map[sample_name] = filtereff;
			}
		}
	}

	return true;
}

float
WeightTool::get_nevents(int dsid) const
{
    std::string dsid_str = std::to_string(dsid);

    for (const auto& entry : m_nevents_map) {
        if (entry.first.find(dsid_str) != std::string::npos)
            return entry.second;
    }

	return -1;
}

float
WeightTool::get_xsection(int dsid) const
{
    std::string dsid_str = std::to_string(dsid);

    for (const auto& entry : m_xsection_map) {
        if (entry.first.find(dsid_str) != std::string::npos)
            return entry.second;
    }

	return -1;
}

float
WeightTool::get_filtereff(int dsid) const
{
    std::string dsid_str = std::to_string(dsid);

    for (const auto& entry : m_filtereff_map) {
        if (entry.first.find(dsid_str) != std::string::npos)
            return entry.second;
    }

	return -1;
}

Double_t
WeightTool::get_lumi_sf(int dsid)
{
    Double_t lumi_SF_fb;

    const float xsection  = this->get_xsection(dsid);
    const float nevents   = this->get_nevents(dsid);
    const float filtereff = this->get_filtereff(dsid);

    if (xsection < 0 || nevents < 0 || filtereff < 0) {
        std::cout << "WARNING (WeightTool.cxx): xsection/nevents/filtereff not found for DSID "
            << dsid << ". Using dummy values..." << std::endl;
        lumi_SF_fb = 1.0;
    } else {
        lumi_SF_fb = xsection * filtereff / nevents;
    }

    return lumi_SF_fb;
}
