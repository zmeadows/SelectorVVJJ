#ifndef Selection_h
#define Selection_h

#include <map>
#include <iostream>
#include <iomanip>
#include <vector>

class VVJJSelector;

class Selection
{
protected:
    const std::string name;
    std::map<std::string, double> cutflow;
    std::vector<std::string> cutflow_order;

public:
    Selection(const std::string& name_) : name(name_) {};
    virtual ~Selection(void) {};

    std::string get_name(void) const { return name; }

    void bump(const std::string& cut_label, const double weight)
    {
        auto cut_found = cutflow.find(cut_label) != cutflow.end();
        if (cut_found) {
            cutflow[cut_label] += weight;
        } else {
            cutflow.emplace(cut_label,weight);
            cutflow_order.push_back(cut_label);
        }
    }

    void print_cutflow(void) {
        size_t max_str_len = 0;
        for (auto const& cut : cutflow_order) {
            if (cut.length() > max_str_len) max_str_len = cut.length();
        }

        std::cout << "### CUTFLOW ###" << std::endl;

        for (auto const& cut : cutflow_order) {
            std::cout << std::setw(max_str_len + 1) << cut << ": " << cutflow[cut] << std::endl;
        }

        std::cout << "###############" << std::endl;
    }

    int get_cut_count(const std::string& cut_label) {
        auto cut_found = cutflow.find(cut_label) != cutflow.end();
        if (!cut_found) {
            return -1;
        } else {
            return cutflow[cut_label];
        }
    }

    virtual bool check(const VVJJSelector* const sel) = 0;
};



#endif
