#ifndef UTIL_H
#define UTIL_H

#include <TH1F.h>
#include <TFile.h>
#include <TKey.h>

#include <string>
#include <vector>
#include <set>
#include <memory>
#include <string>
#include <stdlib.h>
#include <cassert>
#include <string>
#include <dirent.h>

#include <glob.h>
inline std::vector<std::string> glob(const std::string& pat){
    using namespace std;
    glob_t glob_result;
    glob(pat.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> ret;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        ret.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return ret;
}

const Double_t GeV = 1000.;
const Double_t TeV = 1000. * GeV;

void FATAL_ERROR(const std::string& msg);
void WARNING(const std::string& msg);

void print_strings(const std::vector<std::string>& strings);

TH1F* makeTH1F(
        const std::string& name,
        const int num_bins,
        const float x_min,
        const float x_max,
        const std::string& x_axis_title
        );

bool has_suffix(const std::string& str, const std::string& suffix);

inline bool contains(const std::string& substr, const std::string& str) {
    return str.find(substr) != std::string::npos;
}

std::vector<std::string> get_tree_names(const TFile* input_file);

#endif
