#ifndef WeightTool_h
#define WeightTool_h

#include <unordered_map>
#include <string>
#include <vector>
#include <memory>

#include "TH1F.h"

class WeightTool {
public:
    WeightTool();

    virtual ~WeightTool(void) { };

    bool read_weights_file(const std::string& filepath);

    std::unordered_map<std::string, float> m_nevents_map;
    std::unordered_map<std::string, float> m_xsection_map;
    std::unordered_map<std::string, float> m_filtereff_map;

    float get_xsection(int dsid) const;
    float get_nevents(int dsid) const;
    float get_filtereff(int dsid) const;
    Double_t get_lumi_sf(int dsid);
};

#endif // #ifdef WeightTool_h
