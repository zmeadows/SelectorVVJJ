#ifndef VVJJSelector_h
#define VVJJSelector_h

#include <vector>
#include <memory>
#include <string>
#include <unordered_map>
using namespace std;

#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TLorentzVector.h>
#include <TROOT.h>
#include <TSelector.h>

#include "SelectorVVJJ/Selection.h"
#include "SelectorVVJJ/TH1Tagged.h"

// {{{ SELECTIONS
class VVJJSelector;

bool passVVJJBaselineSelection(const VVJJSelector* const sel, Selection* sss);
bool passVVJJTopoSelection(const VVJJSelector* const sel, Selection* sss);
inline bool passVVJJPreSelection(const VVJJSelector* const sel, Selection* sss) {
    return passVVJJBaselineSelection(sel,sss) && passVVJJTopoSelection(sel,sss);
}

bool passWWtag(const VVJJSelector* const sel, Selection* sss);
bool passWZtag(const VVJJSelector* const sel, Selection* sss);
bool passZZtag(const VVJJSelector* const sel, Selection* sss);

class VVJJBaselineSelection : public Selection {
public:
    VVJJBaselineSelection(void) : Selection("VVJJBaselineSelection") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJBaselineSelection(sel, this);
    }
};

class VVJJTopoSelection : public Selection {
public:
    VVJJTopoSelection(void) : Selection("VVJJTopoSelection") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJTopoSelection(sel, this);
    }
};

class VVJJPreSelection : public Selection {
public:
    VVJJPreSelection(void) : Selection("VVJJPreSelection") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelection(sel, this);
    }
};

class VVJJSelectionWW : public Selection {
public:
    VVJJSelectionWW(void) : Selection("VVJJSelectionWW") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelection(sel, this) && passWWtag(sel, this);
    }
};

class VVJJSelectionWZ : public Selection {
public:
    VVJJSelectionWZ(void) : Selection("VVJJSelectionWZ") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelection(sel, this) && passWZtag(sel, this);
    }
};

class VVJJSelectionZZ : public Selection {
public:
    VVJJSelectionZZ(void) : Selection("VVJJSelectionZZ") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelection(sel, this) && passZZtag(sel, this);
    }
};

/*
class VVJJPreSelectionVBFVeto : public Selection {
public:
    VVJJPreSelectionVBFVeto(void) : Selection("VVJJPreSelectionVBFVeto") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelectionVBFVeto(sel, this);
    }
};

bool passVBFqqqqPreSelection(
        const VVJJSelector* const sel,
        Selection* sss,
        const bool do_bumps
        );

bool passVVJJPreSelectionVBFVeto(
        const VVJJSelector* const sel,
        Selection* sss
        );


class VVJJPreSelectionVBFVetoWW : public Selection {
public:
    VVJJPreSelectionVBFVetoWW(void) : Selection("VVJJPreSelectionVBFVetoWW") {};
    bool check(const VVJJSelector* const sel) {
        if (!passVVJJPreSelectionVBFVeto(sel, this))
            return false;
        if (!passWWtag(sel, this))
            return false;
        return true;
    }
};

class VVJJPreSelectionVBFVetoWZ : public Selection {
public:
    VVJJPreSelectionVBFVetoWZ(void) : Selection("VVJJPreSelectionVBFVetoWZ") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelectionVBFVeto(sel, this) && passWZtag(sel, this);
    }
};

class VVJJPreSelectionVBFVetoZZ : public Selection {
public:
    VVJJPreSelectionVBFVetoZZ(void) : Selection("VVJJPreSelectionVBFVetoZZ") {};
    bool check(const VVJJSelector* const sel) {
        return passVVJJPreSelectionVBFVeto(sel, this) && passZZtag(sel, this);
    }
};
*/
// }}}

#define ADDVARDOUBLE(NAME) \
    vars.emplace(NAME,-9999.); \
    branches.emplace(NAME,nullptr); \
    fChain->SetBranchAddress(NAME,&vars.at(NAME),&branches.at(NAME));

#define ADDTH1F(NAME,XMIN,XMAX,BINSIZE,AXISTITLES) \
    hists.emplace(NAME, new TH1Tagged(NAME,XMIN,XMAX,BINSIZE,AXISTITLES));

class VVJJSelector : public TSelector {
public :
   Selection* selection; //!

   TFile* output_file;
   const std::string sample_type;
   const std::string tree_name;

   std::unordered_map<std::string, bool> selection_regions;
   std::unordered_map<std::string, bool> mjj_window_selection_regions;
   std::unordered_map<std::string, bool> prepost_ICHEP_selection_regions;

   void fill_hist_regions(const std::unordered_map<std::string, bool>& regions);

   std::unordered_map<std::string,Double_t> vars; //!
   std::unordered_map<std::string,TBranch*> branches; //!
   std::unordered_map<std::string,TH1Tagged*> hists; //!

   void load_variables(void);

   int num_entries_processed;
   double next_print_percent;

   double lumi_SF_fb;
   double my_weight;
   bool operating_on_mc;

   mutable TLorentzVector tagjet0; //!
   mutable TLorentzVector tagjet1; //!
   void set_tagjet0(const TLorentzVector& v) const { tagjet0 = v; }
   void set_tagjet1(const TLorentzVector& v) const { tagjet1 = v; }

   TTree *fChain;   //!pointer to the analyzed TTree or TChain

   VVJJSelector(
           std::string sample_type_,
           std::string tree_name_,
           TFile* output_file_
           )
       : output_file(output_file_),
       sample_type(sample_type_),
       tree_name(tree_name_),
       num_entries_processed(0),
       next_print_percent(0.0),
       operating_on_mc( 
               sample_type.find("data") == std::string::npos
               && sample_type.find("Data") == std::string::npos
               ),
       fChain(nullptr)
    { }

   virtual ~VVJJSelector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(VVJJSelector,0);
};

#endif

#ifdef VVJJSelector_cxx
void VVJJSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   ADDVARDOUBLE("mc_weight");
   ADDVARDOUBLE("pileup_weight");
   ADDVARDOUBLE("pileup_weightA");
   ADDVARDOUBLE("pileup_weightB");
   if (operating_on_mc) {ADDVARDOUBLE("weight");}
   ADDVARDOUBLE("mc_channelNumber");
   ADDVARDOUBLE("mc_eventNumber");
   ADDVARDOUBLE("lead_jet_pt");
   ADDVARDOUBLE("lead_jet_phi");
   ADDVARDOUBLE("lead_jet_eta");
   ADDVARDOUBLE("lead_jet_m");
   ADDVARDOUBLE("lead_jet_D2");
   ADDVARDOUBLE("lead_jet_ungrtrk500");
   ADDVARDOUBLE("lead_jet_passedWSubstructure50");
   ADDVARDOUBLE("lead_jet_passedWSubstructure80");
   ADDVARDOUBLE("lead_jet_passedZSubstructure50");
   ADDVARDOUBLE("lead_jet_passedZSubstructure80");
   ADDVARDOUBLE("lead_jet_passedWMassCut50");
   ADDVARDOUBLE("lead_jet_passedWMassCut80");
   ADDVARDOUBLE("lead_jet_passedZMassCut50");
   ADDVARDOUBLE("lead_jet_passedZMassCut80");
   ADDVARDOUBLE("first_jet_pt");
   ADDVARDOUBLE("first_jet_eta");
   ADDVARDOUBLE("first_jet_phi");
   ADDVARDOUBLE("first_jet_m");
   ADDVARDOUBLE("first_jet_D2");
   ADDVARDOUBLE("first_jet_ungrtrk500");
   ADDVARDOUBLE("first_jet_passedWSubstructure50");
   ADDVARDOUBLE("first_jet_passedWSubstructure80");
   ADDVARDOUBLE("first_jet_passedZSubstructure50");
   ADDVARDOUBLE("first_jet_passedZSubstructure80");
   ADDVARDOUBLE("first_jet_passedWMassCut50");
   ADDVARDOUBLE("first_jet_passedWMassCut80");
   ADDVARDOUBLE("first_jet_passedZMassCut50");
   ADDVARDOUBLE("first_jet_passedZMassCut80");
   ADDVARDOUBLE("sub_jet_pt");
   ADDVARDOUBLE("sub_jet_phi");
   ADDVARDOUBLE("sub_jet_eta");
   ADDVARDOUBLE("sub_jet_m");
   ADDVARDOUBLE("sub_jet_D2");
   ADDVARDOUBLE("sub_jet_ungrtrk500");
   ADDVARDOUBLE("sub_jet_passedWSubstructure50");
   ADDVARDOUBLE("sub_jet_passedWSubstructure80");
   ADDVARDOUBLE("sub_jet_passedZSubstructure50");
   ADDVARDOUBLE("sub_jet_passedZSubstructure80");
   ADDVARDOUBLE("sub_jet_passedWMassCut50");
   ADDVARDOUBLE("sub_jet_passedWMassCut80");
   ADDVARDOUBLE("sub_jet_passedZMassCut50");
   ADDVARDOUBLE("sub_jet_passedZMassCut80");
   ADDVARDOUBLE("second_jet_pt");
   ADDVARDOUBLE("second_jet_eta");
   ADDVARDOUBLE("second_jet_phi");
   ADDVARDOUBLE("second_jet_m");
   ADDVARDOUBLE("second_jet_D2");
   ADDVARDOUBLE("second_jet_ungrtrk500");
   ADDVARDOUBLE("second_jet_passedWSubstructure50");
   ADDVARDOUBLE("second_jet_passedWSubstructure80");
   ADDVARDOUBLE("second_jet_passedZSubstructure50");
   ADDVARDOUBLE("second_jet_passedZSubstructure80");
   ADDVARDOUBLE("second_jet_passedWMassCut50");
   ADDVARDOUBLE("second_jet_passedWMassCut80");
   ADDVARDOUBLE("second_jet_passedZMassCut50");
   ADDVARDOUBLE("second_jet_passedZMassCut80");
   ADDVARDOUBLE("mjj");
   ADDVARDOUBLE("ptasym");
   ADDVARDOUBLE("dyjj");
   ADDVARDOUBLE("meanY");
   ADDVARDOUBLE("met");
   ADDVARDOUBLE("pass_HLT_j420_a10_lcw_L1J100");
   ADDVARDOUBLE("pass_vvjj_lepton_veto");
   ADDVARDOUBLE("pass_grl");
   ADDVARDOUBLE("pass_grlA");
   ADDVARDOUBLE("pass_grlB");
   ADDVARDOUBLE("pass_evtclean");
   ADDVARDOUBLE("pass_jetclean");
   ADDVARDOUBLE("run");
   ADDVARDOUBLE("event");
   ADDVARDOUBLE("n_muons");
   ADDVARDOUBLE("n_elecs");
   ADDVARDOUBLE("n_jets");

   ADDTH1F("h_lead_jet_pt"     , 2*36 , 400   , 4000 , "Leading jet p_{T} [GeV]");
   ADDTH1F("h_lead_jet_eta"    , 40   , -2    , 2    , "Leading jet #eta");
   ADDTH1F("h_lead_jet_phi"    , 100  , -3.15 , 3.15 , "Leading jet #phi");
   ADDTH1F("h_lead_jet_m"      , 200  , 0     , 200  , "Leading jet m_{comb} [GeV]");
   ADDTH1F("h_lead_jet_D2"     , 100  , 0     , 5    , "Leading jet D2");
   ADDTH1F("h_lead_jet_ntrk"   , 150  , 0     , 150  , "Leading jet n_{trk}");
   ADDTH1F("h_sub_jet_pt"      , 2*36 , 400   , 4000 , "Subleading jet p_{T} [GeV]");
   ADDTH1F("h_sub_jet_eta"     , 40   , -2    , 2    , "Subleading jet #eta");
   ADDTH1F("h_sub_jet_phi"     , 100  , -3.15 , 3.15 , "Subleading jet #phi");
   ADDTH1F("h_sub_jet_m"       , 200  , 0     , 200  , "Subleading jet m_{comb} [GeV]");
   ADDTH1F("h_sub_jet_D2"      , 100  , 0     , 5    , "Subleading jet D2");
   ADDTH1F("h_sub_jet_ntrk"    , 150  , 0     , 150  , "Subleading jet n_{trk}");
   ADDTH1F("h_first_jet_pt"    , 2*36 , 400   , 4000 , "First mass-ordered jet p_{T} [GeV]");
   ADDTH1F("h_first_jet_eta"   , 40   , -2    , 2    , "First mass-ordered jet #eta");
   ADDTH1F("h_first_jet_phi"   , 100  , -3.15 , 3.15 , "First mass-ordered jet #phi");
   ADDTH1F("h_first_jet_m"     , 200  , 0     , 200  , "First mass-ordered jet m_{comb} [GeV]");
   ADDTH1F("h_first_jet_D2"    , 100  , 0     , 5    , "First mass-ordered jet D2");
   ADDTH1F("h_first_jet_ntrk"  , 150  , 0     , 150  , "First mass-ordered jet n_{trk}");
   ADDTH1F("h_second_jet_pt"   , 2*36 , 400   , 4000 , "Second mass-ordered jet p_{T} [GeV]");
   ADDTH1F("h_second_jet_eta"  , 40   , -2    , 2    , "Second mass-ordered jet #eta");
   ADDTH1F("h_second_jet_phi"  , 100  , -3.15 , 3.15 , "Second mass-ordered jet #phi");
   ADDTH1F("h_second_jet_m"    , 200  , 0     , 200  , "Second mass-ordered jet m_{comb} [GeV]");
   ADDTH1F("h_second_jet_D2"   , 100  , 0     , 5    , "Second mass-ordered jet D2");
   ADDTH1F("h_second_jet_ntrk" , 150  , 0     , 150  , "Second mass-ordered jet n_{trk}");
   ADDTH1F("h_mjj"             , 50   , 1000  , 6000 , "m_{JJ} [GeV]");
   ADDTH1F("h_ptasym"          , 100  , 0     , 1    , "p_{T} asymmetry");
   ADDTH1F("h_dyjj"            , 250  , 0     , 10   , "|#Delta y|");
   ADDTH1F("h_ntrkEffNum"      , 100  , 0     , 100  , "Leading jet n_{trk}");
   ADDTH1F("h_ptasymEffNum"    , 100  , 0     , 1    , "p_{T} asymmetry");
   ADDTH1F("h_dyjjEffNum"      , 100  , 0     , 5    , "|#Delta y|");
   ADDTH1F("h_meanY"           , 250  , 0     , 10   , "<|y|>");

   // ADDTH1F("h_lead_tagjet_pt"  , 2*26 , 400   , 3000 , ";Leading tag jet p_{T} [GeV]");
   // ADDTH1F("h_lead_tagjet_eta" , 80   , -4    , 4    , ";Leading tag jet #eta");
   // ADDTH1F("h_lead_tagjet_phi" , 100  , -3.15 , 3.15 , ";Leading tag jet #phi");
   // ADDTH1F("h_lead_tagjet_m"   , 100  , 50    , 2000 , ";Leading tag jet m_{comb} [GeV]");
   // ADDTH1F("h_sub_tagjet_pt"   , 2*26 , 400   , 3000 , ";Subleading tag jet p_{T} [GeV]");
   // ADDTH1F("h_sub_tagjet_eta"  , 80   , -4    , 4    , ";Subleading tag jet #eta");
   // ADDTH1F("h_sub_tagjet_phi"  , 100  , -3.15 , 3.15 , ";Subleading tag jet #phi");
   // ADDTH1F("h_sub_tagjet_m"    , 100  , 50    , 2000 , ";Subleading tag jet m_{comb} [GeV]");
   // ADDTH1F("h_tagjet_mjj"      , 30   , 0     , 3000 , ";m_{JJ}^{tag} [GeV]");
   // ADDTH1F("h_tagjet_dy"       , 100  , -5    , 5    , ";m_{JJ}^{tag} [GeV]");
   // ADDTH1F("h_tagjet_deta"     , 100  , -5    , 5    , ";m_{JJ}^{tag} [GeV]");
   // ADDTH1F("h_tagjet_dphi"     , 100  , -5    , 5    , ";m_{JJ}^{tag} [GeV]");
}

Bool_t VVJJSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef VVJJSelector_cxx
