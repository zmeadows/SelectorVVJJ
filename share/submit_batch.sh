#!/bin/zsh
set -e

DRY_RUN=true

INPUT_DIR=/afs/cern.ch/work/z/zmeadows/public/VVJJ/LocalTest/09032017_PrePostICHEP_Ntuples_CATCH_BROKEN_WZ2000GeV_MISSING_RS1500/ntuples
JOB_SCRIPT=/afs/cern.ch/work/z/zmeadows/public/VVJJ/SelectorVVJJ/SelectorVVJJ/share/batch_job.sh

for INPUT_FILE in $(find $INPUT_DIR -type f -name '*tree_0.root'); do
        OUTPUT_FILE=${INPUT_FILE%.root*}.cp.root

        if [[ $INPUT_FILE == *"Data"* ]] then
            SAMPLE_TYPE="data16"
        elif [[ $INPUT_FILE == *"HVT_Agv1_VcWZ_qqqq"* ]] then
            MASS_POINT=${INPUT_FILE##*_m}
            MASS_POINT=${MASS_POINT%%_*}
            SAMPLE_TYPE=HVT_Agv1_VcWZ_qqqq_m$MASS_POINT
        elif [[ $INPUT_FILE == *"HVT_Agv1_VzWW_qqqq"* ]] then
            MASS_POINT=${INPUT_FILE##*_m}
            MASS_POINT=${MASS_POINT%%_*}
            SAMPLE_TYPE=HVT_Agv1_VzWW_qqqq_m$MASS_POINT
        elif [[ $INPUT_FILE == *"RS_G_ZZ_qqqq_c10"* ]] then
            MASS_POINT=${INPUT_FILE##*_m}
            MASS_POINT=${MASS_POINT%%_*}
            SAMPLE_TYPE=RS_G_ZZ_qqqq_c10_m$MASS_POINT
        elif [[ $INPUT_FILE == *"RS_G_WW_qqqq_c10"* ]] then
            MASS_POINT=${INPUT_FILE##*_m}
            MASS_POINT=${MASS_POINT%%_*}
            SAMPLE_TYPE=RS_G_WW_qqqq_c10_m$MASS_POINT
        elif [[ $INPUT_FILE == *"QCD"* ]] then
            SAMPLE_TYPE="qcd"
        else
            echo "ERROR: unknown sample type for file: ${INPUT_FILE}"
        fi


        if [ $DRY_RUN ]
        then
            job_cmd="vvjj_selector --input $INPUT_FILE --output $OUTPUT_FILE --tree nominal --selection NoSelection --label $SAMPLE_TYPE > $OUTPUT_FILE.log 2>&1"
            echo $job_cmd
        else
            job_cmd="INPUT_FILE=$INPUT_FILE OUTPUT_FILE=$OUTPUT_FILE SAMPLE_TYPE=$SAMPLE_TYPE $JOB_SCRIPT"
            eval "bsub -R \"pool>3000\" -q 8nh \"$job_cmd\""
        fi
done
