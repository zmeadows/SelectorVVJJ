#!/bin/zsh

#PBS -W umask=022
#PBS -j oe

LUMINOSITY=25.0
SELECTION=NoSelection

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

cd /afs/cern.ch/work/z/zmeadows/public/VVJJ/SelectorVVJJ
lsetup 'rcsetup -u'
lsetup rcsetup

job_cmd="vvjj_selector --input $INPUT_FILE --output $OUTPUT_FILE --tree nominal --selection $SELECTION --label $SAMPLE_TYPE --luminosity $LUMINOSITY > $OUTPUT_FILE.log 2>&1"

eval $job_cmd
#echo $job_cmd
